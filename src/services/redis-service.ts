import { injectable } from "inversify";
import { RedisAPI } from "../@types/redis-service";
import * as Redis from "ioredis";
import { RedisOptions } from "ioredis";
import { EventEmitter } from "events";
import { promisify } from "util";
import { RedisMessage } from "../@types/redis-message";

export enum SubChannels {
  Rolls = "rollsUpdated",
  Players = "playersUpdated",
  Characters = "charactersUpdated",
}

export enum PubChannels {
  Rolls = "updateRolls",
  Players = "updatePlayers",
  Characters = "updateCharacters",
}
export class RedisTimeoutError extends Error {}
@injectable()
export class RedisService extends EventEmitter implements RedisAPI {
  // Setting infinite retries by default
  private static readonly DEFAULT_OPTIONS: RedisOptions = {
    maxRetriesPerRequest: null,
  };
  //We actually need two separate client to pub and sub
  private subscriber: Redis.Redis;
  private publisher: Redis.Redis;
  private connected = true;
  private options: RedisOptions;

  constructor(options?: Redis.RedisOptions) {
    super();
    this.options = Object.assign(RedisService.DEFAULT_OPTIONS, options);
    this.subscriber = new Redis(this.options);
    this.publisher = new Redis(this.options);
    // Message mirroring
    this.subscriber.on("message", (channel, message) =>
      this.emit("message", [channel, message])
    );
    //
    this.subscriber.on("error", (err) => {
      if (this.connected) {
        console.error(
          "Could not connect to Redis Db, exponentially backing off"
        );
        this.connected = false;
      }

      console.error(err.message);
    });
  }

  publish(channel: string, payload: Record<string, any>): void {
    this.publisher.publish(channel, JSON.stringify(payload));
  }

  async subscribe(channel: string): Promise<void> {
    const subProm = promisify(this.subscriber.subscribe).bind(this.subscriber);
    try {
      await subProm(channel);
    } catch (e) {
      console.error(e);
    }
  }

  async unsubscribe(channel: string): Promise<void> {
    await this.subscriber.unsubscribe(channel);
  }

  public async waitFor(channel: string): Promise<RedisMessage> {
    return new Promise((res, rej) => {
      this.subscribe(channel);
      this.on("message", (event) => {
        const channel = event[0];
        const message = event[1];
        if (channel === channel) {
          res(JSON.parse(message));
        }
      });
      setTimeout(() => {
        rej("Timemout !");
      }, 120000);
    });
  }
}
