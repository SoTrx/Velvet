import { Player } from "./api";

export interface ScrapperServiceAPI {
  retrievePlayersOfCampaign(campaignRoll20Id: number): Promise<Player[]>;
  joinGame(joinUrl: string): Promise<void>;
}
