import "reflect-metadata";
import { container } from "../inversify.config";
import { TYPES } from "../types";
import { ThumbnailGeneratorAPI } from "../@types/thumbnail-generator-API";

describe("Font Manager", () => {
  const thumbnailGenerator = container.get<ThumbnailGeneratorAPI>(
    TYPES.ThumbnailService
  );
  it("Should get the thumbnail of an existing session", async () => {
    const path = await thumbnailGenerator.generateThumbnailForSession(288);
    console.log(path);
  }, 10000);
  it("Should send an error on invalid session id", async () => {
    try {
      const path = await thumbnailGenerator.generateThumbnailForSession(999999);
      console.log(path);
    } catch (e) {
      console.log("error");
    }
  }, 10000);
  it("Should generate a sample thumbnail for a campain ", async () => {
    const path = await thumbnailGenerator.generateSampleThumbnailForCampaign(
      8,
      "This title is incredible"
    );
    console.log(path);
  }, 10000);
});
