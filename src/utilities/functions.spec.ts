import { debounce, throttle } from "./functions";
import { EventEmitter } from "events";

describe("Functions Utilities", () => {
  describe("Debouncer", () => {
    // Taken from https://github.com/chodorowicz/ts-debounce/blob/master/src/index.test.ts
    it("Should properly debounces function", () => {
      jest.useFakeTimers();

      const func = jest.fn();
      const debouncedFunction = debounce(func, 100);

      debouncedFunction();
      expect(func).not.toBeCalled();

      jest.runTimersToTime(50);
      expect(func).not.toBeCalled();

      jest.runTimersToTime(100);
      expect(func).toBeCalled();
      expect(func.mock.calls.length).toBe(1);
    });

    it("Should properly debounce function with isImmediate set to true ", () => {
      jest.useFakeTimers();

      const func = jest.fn();
      const debouncedFunction = debounce(func, 100, { isImmediate: true });

      debouncedFunction();
      expect(func).toBeCalled();
      expect(func.mock.calls.length).toBe(1);

      jest.runTimersToTime(50);
      expect(func.mock.calls.length).toBe(1);

      jest.runTimersToTime(100);
      expect(func.mock.calls.length).toBe(1);
    });
  });

  describe("Throttler", () => {
    class Garbage extends EventEmitter {
      constructor() {
        super();
        setInterval(() => this.emit("debounceTest"), 100);
      }
    }
    it("Should throttle events", () => {
      jest.useFakeTimers();

      const garbage = new Garbage();
      let cmpt = 0;
      const throttled = throttle(() => cmpt++, 1000);
      garbage.on("debounceTest", throttled);
      jest.runTimersToTime(150);
      expect(cmpt).toEqual(1);
      jest.runTimersToTime(1400);
      expect(cmpt).toEqual(2);
    });
  });
});
