import "reflect-metadata";
import { container } from "../../inversify.config";
import { DataService } from "../../@types/data-service";
import { TYPES } from "../../types";
import { MockDataService } from "../../services/mock/mock-data-service";
import DisplayCampaigns from "./display-campaigns";
import { CommandoClient } from "discord.js-commando";

jest.mock("discord.js-commando");

describe("displayCampaigns", () => {
  const command = new DisplayCampaigns(CommandoClient);
  beforeAll(() => {
    container.rebind<DataService>(TYPES.DataService).to(MockDataService);
  });

  it("Should display campaigns without crashing", async () => {
    const campaigns = await command.getCampaigns();
    for (const c of campaigns) {
      console.log(`Processing ${c.name}...`);
      await command.toEmbedData(c, new CommandoClient());
      console.log(`Ok !`);
    }
    return;
  }, 90000);
});
