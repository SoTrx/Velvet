import { EventEmitter } from "events";
import { youtube_v3 } from "googleapis";
import { YoutubeAPI, YoutubeItemOptions } from "../../@types/youtube-service";
import { injectable } from "inversify";
import Schema$Playlist = youtube_v3.Schema$Playlist;
import Schema$PlaylistItem = youtube_v3.Schema$PlaylistItem;
import Schema$LiveBroadcast = youtube_v3.Schema$LiveBroadcast;
import Schema$Video = youtube_v3.Schema$Video;
import Schema$ThumbnailSetResponse = youtube_v3.Schema$ThumbnailSetResponse;

@injectable()
export class MockYoutubeService extends EventEmitter implements YoutubeAPI {
  getIdFromUrl(url: string): string {
    return url.substring(0, 3);
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async createPlaylist(options: YoutubeItemOptions): Promise<Schema$Playlist> {
    return {
      id: "DDDDDD",
    };
  }

  async addVideoToPlaylist(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    videoId: string,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    playlistId: string
  ): Promise<Schema$PlaylistItem> {
    return null;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async deletePlaylist(playlistId: string): Promise<void> {
    return;
  }

  async getCurrentBroadcast(): Promise<Schema$LiveBroadcast> {
    return null;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async getVideoDuration(id: string): Promise<number> {
    return 0;
  }

  updateThumbnail(
    id: string,
    path: string
  ): Promise<Schema$ThumbnailSetResponse> {
    return Promise.resolve(undefined);
  }

  async updateVideo(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    id: string,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    parameters: YoutubeItemOptions
  ): Promise<void> {
    return;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async deleteVideo(videoId: string): Promise<void> {
    return;
  }

  async uploadVideo(
    path: string,
    options: YoutubeItemOptions
  ): Promise<Schema$Video> {
    return Promise.resolve(undefined);
  }
}
