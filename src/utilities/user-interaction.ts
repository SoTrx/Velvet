import { GlobalExt } from "../@types/global";
import {
  GuildMember,
  Message,
  MessageAttachment,
  TextChannel,
} from "discord.js";
import { EventEmitter } from "events";
import { CommandoClient, CommandoMessage } from "discord.js-commando";
import axios from "axios";
import { createWriteStream } from "fs";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type testFunctionType = (...args: Array<any>) => boolean;
declare const global: GlobalExt;

export async function downloadAttachment(
  attachment: MessageAttachment,
  path: string
) {
  const data = (
    await axios.get(attachment.url, {
      responseType: "stream",
    })
  ).data;
  const writer = createWriteStream(path);
  await data.pipe(writer);
  return new Promise((resolve, reject) => {
    writer.on("finish", () => resolve(path));
    writer.on("error", reject);
  });
}
export function progressBar(value: number, maxValue: number, size: number) {
  const percentage = value / maxValue;
  const progress = Math.round(size * percentage);
  const emptyProgress = size - progress;

  const progressText = "▇".repeat(progress);
  const emptyProgressText = "—".repeat(emptyProgress);
  const percentageText = Math.round(percentage * 100) + "%";
  return "**[" + progressText + emptyProgressText + "]" + percentageText + "**";
}
/**
 * @summary Wait for a user to post a valid message
 * @param  authorId who are we waiting for ?
 * @param  validation is the given message valid ? Returns True or False
 * @param timeout Reject automatically after a certain time (-1 for infinite)
 * @returns Resolve with the message the user sent.
 */
// tslint:disable-next-line:only-arrow-functions
export async function waitForMessage(
  client: CommandoClient,
  authorId: string,
  validation: testFunctionType,
  timeout: number
): Promise<Message> {
  let resolve;
  let reject;

  // We actually need the function() for arguments to be defined
  // tslint:disable-next-line:only-arrow-functions
  const promLock: Promise<Message> = new Promise<Message>(function (): void {
    // eslint-disable-next-line prefer-rest-params
    [resolve, reject] = arguments;
  });

  const func = (message: Message): void => {
    if (message.author.id === authorId) {
      if (validation(message.content, message)) {
        client.off("message", func);
        resolve(message);
      }
    }
  };

  if (timeout !== -1) {
    setTimeout(() => {
      client.off("message", func);
      reject(new Error("timeout"));
    }, timeout);
  }

  client.on("message", func);

  return promLock;
}

/**
 * @summary Ask a user a yes / no question
 * @param evt General handle to text channel
 * @param  authorId Id of the one user that has to answer
 * @param question question to ask
 * @param timeout timeout for the question. (Default is 5') Null is then returned.
 * @returns true for yes, false for no, null if timeout
 */
// tslint:disable-next-line:only-arrow-functions
export async function yesNoQuestion(
  evt: CommandoMessage,
  authorId: string,
  question: string,
  timeout?: number
): Promise<boolean> {
  await evt?.reply(`${question} [O/n]`);
  const messageValidation = (message: string): boolean => {
    const validResponses = ["O", "o", "n", "N", "Oui", "Non", "oui", "non"];
    const isValid = validResponses.includes(message);
    if (!isValid) {
      evt?.reply("C'est oui ou non...").catch(undefined);
    }

    return isValid;
  };

  let chosenMessage;
  try {
    chosenMessage = await waitForMessage(
      evt?.client as CommandoClient,
      authorId,
      messageValidation,
      timeout || 5 * 60 * 1000
    );
  } catch (e) {
    // Return "no" on timeout
    return false;
  }
  const positiveAnswers = ["O", "o", "Oui", "oui"];
  return positiveAnswers.includes(chosenMessage.content);
}

export interface ChooseOneItemOptions {
  /** what to reply if no item is found */
  noItemResponse?: string;
  /** question's timeout */
  timeout?: number;
  /** Whether to display the choices to the user */
  displayChoices?: boolean;
}

/**
 * @summary Make the user choose between the choices in itemList
 * @param evt General handle to text channel
 * @param itemList Choices. Each choice has to have a .toString() method
 * @param displayProperty Property to display
 * @param question Question to ask the user
 * @param options optional parameters
 * @returns chosen item or null if no item, stopped or timeout
 */
// tslint:disable-next-line:only-arrow-functions
export async function chooseOneItem<T, M>(
  evt: CommandoMessage,
  itemList: Array<T>,
  displayProperty: keyof T,
  question: string,
  options: ChooseOneItemOptions = {
    displayChoices: true,
    timeout: -1,
  }
): Promise<T> {
  if (itemList.length === 0) {
    await evt.reply(options.noItemResponse || `Il n'y a rien à choisir!`);

    return undefined;
  }

  let chosenItem;
  if (itemList.length === 1) {
    chosenItem = itemList[0];
  } else {
    let choicestring = `${question} (staph pour annuler) \n`;

    if (options && options.displayChoices) {
      for (const [index, campaign] of itemList.entries()) {
        choicestring += `\t\t**${index + 1}**)\t-->\t`;
        choicestring += `${await campaign[displayProperty]}\n`;
      }
    }

    await evt.channel.send(choicestring);
    const choice = await waitForMessage(
      evt.client as CommandoClient,
      evt.author.id,
      (message) => {
        if (message === "cancel") {
          return true;
        }
        const choiceIndex = parseInt(message, 10);
        if (choiceIndex > itemList.length || choiceIndex < 1) {
          evt
            .reply(
              `Je te dis entre 1 et ${itemList.length} et tu me réponds ${choiceIndex}...`
            )
            .catch(console.error);
          return false;
        }
        if (isNaN(choiceIndex) || !isFinite(choiceIndex)) {
          evt.reply("Euh, chelou toi, on annule tout !").catch(console.error);
          message = "cancel";
          return true;
        }

        return true;
      },
      (options && options.timeout) || 5 * 60 * 100
    ).catch(undefined);

    if (choice.content === "cancel") {
      await evt.channel.send("Commande annulée !");

      return undefined;
    }
    chosenItem = itemList[parseInt(choice.content, 10) - 1];
  }

  return chosenItem;
}

/**
 * Display a self updating message on a discord text channel listening to event
 * and reacting to it
 * @param observable Object to observe events from
 * @param watchedEvent event to trigger the message
 * @param messageHandle Function returning the formatted message given the data returned from the event.
 * @param textChannel TextChannel to message into
 * @param endEvent Event name to listen to to stop updating the message
 * @param updateInterval Debounce interval for discord actual updates
 */
export async function updatingMessage<T extends EventEmitter>(
  observable: T,
  watchedEvent: string,
  messageHandle: (data) => string,
  textChannel: TextChannel,
  endEvent: string,
  updateInterval = 3000
): Promise<void> {
  let message = messageHandle("En cours");
  const discordMessage = await textChannel.send(message);
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  const buildMessage = (data) => (message = messageHandle(data));
  observable.on(watchedEvent, (data) => buildMessage(data));
  const intervalId = setInterval(
    () => discordMessage.edit(message),
    updateInterval
  );
  observable.once(endEvent, async () => {
    observable.off(watchedEvent, buildMessage);
    clearInterval(intervalId);
    await discordMessage.edit(messageHandle("Terminé !"));
  });
}

export async function displayTimer(
  initialTime: number,
  textChannel: TextChannel,
  messageHandle: (timer: string) => string,
  updateInterval = 5000
): Promise<void> {
  const toFormat = (secondsRemaining: number): string => {
    let formatttedMinutes: string | number = ~~((secondsRemaining % 3600) / 60);
    let formatttedSeconds: string | number = Math.round(secondsRemaining % 60);
    //Format to mm:ss format
    if (formatttedSeconds < 10) {
      formatttedSeconds = `0${formatttedSeconds}`;
    } else if (formatttedSeconds == 60) {
      ++formatttedMinutes;
      formatttedSeconds = `00`;
    }
    if (formatttedMinutes < 10) {
      formatttedMinutes = `0${formatttedMinutes}`;
    }
    return `${formatttedMinutes}:${formatttedSeconds}`;
  };
  const discordMessage = await textChannel.send(
    messageHandle(toFormat(initialTime))
  );
  const startTime = new Date().getTime();
  const routineId = setInterval(() => {
    const secondsRemaining =
      (initialTime - (new Date().getTime() - startTime)) / 1000;
    if (secondsRemaining >= 0) {
      discordMessage.edit(messageHandle(toFormat(secondsRemaining)));
    } else {
      clearInterval(routineId);
    }
  }, updateInterval);
}

export async function waitForMentionedUsers(
  client: CommandoClient,
  evt: CommandoMessage,
  userId: string
): Promise<GuildMember[]> {
  const validation = (content: string, message: CommandoMessage) => {
    const isValid = message.mentions?.members?.array().length > 0;
    if (!isValid)
      message.channel.send("Aucun membre mentionné dans ce message !");
    return isValid;
  };
  const chosenMessage = await waitForMessage(client, userId, validation, -1);
  return chosenMessage.mentions.members.array();
}
