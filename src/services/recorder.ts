import { inject, injectable, decorate, interfaces } from "inversify";
import {
  AccurateTime,
  DiscordRecorderAPI,
  MainRecorderAPI,
  Roll20RecorderAPI,
} from "../@types/recording-service";
import * as ffmpeg from "fluent-ffmpeg";
import { FfmpegCommand } from "fluent-ffmpeg";
import { VoiceChannel } from "discord.js";
import { createWriteStream, renameSync } from "fs";
import { tmpdir } from "os";
import { join } from "path";
import { Campaign } from "../@types/api";
import { EventEmitter } from "events";
import { TYPES } from "../types";
import { throttle } from "../utilities/functions";
import Factory = interfaces.Factory;

export enum RecordingMode {
  AudioOnly,
  VideoAndAudio,
}
decorate(injectable(), EventEmitter);
@injectable()
export class RecorderService extends EventEmitter implements MainRecorderAPI {
  public recordingPath: string;
  private videoRecorder: Roll20RecorderAPI;
  private tempRecordingAudio: string;
  private tempRecordingVideo: string;

  public recordingMode: RecordingMode;
  private streamProcess: FfmpegCommand;
  private allowSubProcessToBeKilled = false;
  private startTime: Date;
  private calculatedOffset = 0;

  constructor(
    @inject(TYPES.AudioRecorder) private audioRecorder: DiscordRecorderAPI,
    @inject(TYPES.VideoRecorderFactory)
    private videoRecorderFactory: (videoRecorder: string) => Roll20RecorderAPI
  ) {
    super();
    const now = Date.now();
    this.tempRecordingAudio = join(tmpdir(), `audio-recording-${now}.flac`);
    this.tempRecordingVideo = join(tmpdir(), `video-recording-${now}.flv`);
    this.recordingPath = join(tmpdir(), `recording-${now}`);
  }

  startStream(recordingMode: RecordingMode): void {
    const output = createWriteStream(this.tempRecordingVideo);
    this.streamProcess = ffmpeg();
    switch (recordingMode as RecordingMode) {
      case RecordingMode.AudioOnly:
        this.streamProcess
          .input("color=black:s=1280x720:r=25")
          .inputFormat("lavfi")
          .inputFormat("-i anullsrc=r=48000:cl=stereo")
          .map("-map 1:a")
          .map("-map 0:v")
          .outputOption("-c:v libx264rgb");

        break;
      case RecordingMode.VideoAndAudio:
        this.streamProcess
          .input(this.videoRecorder.recordingPath)
          .outputOption("-c:v copy");
        break;
    }
    this.streamProcess
      .addOption("-analyzeduration 0")
      .outputOption("-preset ultrafast")
      .format("flv")
      .on("error", (err) => {
        if (this.allowSubProcessToBeKilled) {
          this.allowSubProcessToBeKilled = false;
        }
        console.error(err);
      })
      .on("start", (cmdLine) => console.log(cmdLine))
      .output(output);
    this.streamProcess.run();
  }

  stopStream(): void {
    this.streamProcess.kill("SIGKILL");
  }

  async startRecording(
    voiceChannel: VoiceChannel,
    campaign: Campaign,
    videoRecorder: "vm" | "roll20"
  ): Promise<void> {
    this.videoRecorder = this.videoRecorderFactory(videoRecorder);
    this.audioRecorder.startRecording(voiceChannel.id);
    this.recordingMode = RecordingMode.VideoAndAudio;
    try {
      await this.videoRecorder.startRecording(campaign);
    } catch (e) {
      this.recordingMode = RecordingMode.AudioOnly;
    }
    this.startStream(this.recordingMode);
    this.startTime = new Date();
  }

  async stopRecording(): Promise<number> {
    this.allowSubProcessToBeKilled = true;
    this.stopStream();
    const audioStart = await this.audioRecorder.stopRecording();
    if (this.recordingMode === RecordingMode.VideoAndAudio) {
      const videoStart = await this.videoRecorder.stopRecording();
      if (audioStart !== undefined && videoStart !== undefined) {
        console.log(`Audio start: ${audioStart}`);
        console.log(`Video start: ${videoStart}`);
        this.calculatedOffset = this.calculateOffset(videoStart, audioStart);
        //this.calculatedOffset = 0;
        console.log(`calculated offset : ${this.calculatedOffset}`);
      }
    }
    return Math.round((new Date().getTime() - this.startTime.getTime()) / 1000);
  }

  calculateOffset(video: AccurateTime, audio: AccurateTime): number {
    const NS_PER_SEC = 1e9;
    let seconds = video[0] - audio[0];
    let nanosecs = video[1] - audio[1];

    if (nanosecs < 0) {
      seconds -= 1;
      nanosecs += NS_PER_SEC;
    }
    return Number(`${seconds}.${String(nanosecs).substr(0, 3)}`);
  }

  async processRecording(): Promise<string> {
    //await this.downloadRecordedAudio();
    const tracks = await this.downloadRecordedAudios();
    await this.concatAudios(tracks);
    await this.mixRecordersResults();
    this.calculatedOffset = 0;
    return this.recordingPath;
  }

  private async downloadRecordedAudios(): Promise<string[]> {
    let size = 0;
    const throttledProgress = throttle(
      (progress) => this.emit("audioDownloadProgress", progress),
      3000
    );
    const audios = await this.audioRecorder.getAudioStreams();
    return Promise.all(
      audios.map((stream, index) => {
        return new Promise<string>(async (res, rej) => {
          const output = createWriteStream(
            `${this.tempRecordingAudio}-${index}`
          );
          stream.on("data", (chunk) => output.write(Buffer.from(chunk)));
          stream.on("data", (chunk: Buffer) => {
            size += chunk.length;
            throttledProgress(size);
          });
          stream.on("end", () => res(`${this.tempRecordingAudio}-${index}`));
          stream.on("error", rej);
        });
      })
    );
  }

  private async concatAudios(audios: string[]) {
    const throttledProgress = throttle(
      (progress) => this.emit("audioConcatProgress", progress),
      3000
    );
    // If there is just one audio track, we don't need to do anything.
    if (audios.length === 1) {
      renameSync(audios[0], this.tempRecordingAudio);
      return;
    }

    //ffmpeg -i ./part0.ogg -i part1.ogg  -filter_complex '[0][1]concat=n=2:v=0:a=1[out]' -map [out] output.ogg
    return new Promise((res, rej) => {
      const command = ffmpeg();
      // Add all audio tracks
      audios.forEach((a) => command.input(a));

      // Forge a string like "'[0][1]concat=n=2:v=0:a=1[out]'"
      const concatFilter = `${audios
        .map((a, index) => `[${index}]`)
        .join("")}concat=n=${audios.length}:v=0:a=1[out]`;
      command
        .complexFilter(concatFilter)
        .map("[out]")
        .on("error", rej)
        .on("end", res)
        //For some reason the progress emitted can go over 100%
        .on("progress", (p) =>
          throttledProgress(Math.min(Number(p.percent), 100))
        )
        .format("flac")
        .saveToFile(this.tempRecordingAudio);
    });
  }

  private async downloadRecordedAudio(): Promise<void> {
    let size = 0;
    const throttledProgress = throttle(
      (progress) => this.emit("audioDownloadProgress", progress),
      3000
    );
    return new Promise(async (res, rej) => {
      const output = createWriteStream(this.tempRecordingAudio);
      const stream = await this.audioRecorder.getAudioStream();
      stream.on("data", (chunk) => output.write(Buffer.from(chunk)));
      stream.on("data", (chunk: Buffer) => {
        size += chunk.length;
        throttledProgress(size);
      });
      stream.on("end", res);
      stream.on("error", rej);
    });
  }

  private async mixRecordersResults(): Promise<void> {
    const throttledProgress = throttle(
      (progress) => this.emit("recorderMixingProgress", progress),
      3000
    );
    return new Promise((res, rej) => {
      //Add video
      const command = ffmpeg().input(this.tempRecordingVideo);

      // If video is ahead, skip a part of it
      if (this.calculatedOffset < 0)
        command.inputOption(`-ss ${Math.abs(this.calculatedOffset)}`);

      // Add audio
      command.input(this.tempRecordingAudio);

      // If audio is ahead, add a skip a part of it
      if (this.calculatedOffset > 0)
        command.inputOption(`-ss ${this.calculatedOffset}`);

      command
        .complexFilter(
          "[0:a]loudnorm=I=-16:TP=-1.5:LRA=11, aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo[r1];[1]loudnorm=I=-16:TP=-1.5:LRA=11,asplit=2[sc][v1];[r1][sc]sidechaincompress=threshold=0.05:ratio=5:level_sc=0.8[bg];[bg][v1]amix=weights=0.2 1[a3]"
        )
        .map("[a3]")
        .outputOption("-map 0:v")
        .outputOption("-c:v copy")
        .on("error", rej)
        .on("end", res)
        //For some reason the progress emitted can go over 100%
        .on("progress", (p) =>
          throttledProgress(Math.min(Number(p.percent), 100))
        )
        .format("mp4")
        .saveToFile(this.recordingPath);
    });
  }
}
