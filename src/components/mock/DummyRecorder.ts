import { AccurateTime, RecorderAPI } from "../../@types/recording-service";
import { injectable } from "inversify";
import { RecordingMode } from "../../services/recorder";

@injectable()
export class DummyRecorder implements RecorderAPI {
  recordingMode: RecordingMode.AudioOnly;
  recordingPath: string;

  startRecording(): void {
    return;
  }

  async stopRecording(): Promise<AccurateTime> {
    return;
  }

  getDevices(): string[] {
    return [];
  }
}
