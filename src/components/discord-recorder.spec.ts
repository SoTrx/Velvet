import "reflect-metadata";
import { container } from "../inversify.config";
import { DiscordRecorderAPI } from "../@types/recording-service";
import { TYPES } from "../types";
import { createHash } from "crypto";
import { readFileSync } from "fs";
import { resolve } from "path";

describe("Discord recorder", () => {
  const SAMPLE_VOICE_CHANNEL_ID = "1";
  const SAMPLE_RECORD_ID = "872660673";
  const recorder = container.get<DiscordRecorderAPI>(TYPES.AudioRecorder);
  it("Should get the resulting audio Stream", async () => {
    const downloadPromise = new Promise(async (res, rej) => {
      recorder.recordId = SAMPLE_RECORD_ID;
      const stream = await recorder.getAudioStream();
      stream.on("error", (e) => rej(e));
      stream.once("data", (chunk) => res(chunk));
    });
    await downloadPromise;
  });
  it(
    "Should download the whole file",
    async () => {
      const originalHash = "e26ce5de420d99c161f682fc375ae8ae";
      const testHash = createHash("md5");
      const hashPromise = new Promise(async (res, rej) => {
        recorder.recordId = SAMPLE_RECORD_ID;
        const stream = await recorder.getAudioStream();
        stream.on("error", (e) => rej(e));
        stream.on("data", (chunk) => testHash.update(chunk));
        stream.once("end", () => res(testHash.digest("hex")));
      });
      const completedHash = await hashPromise;

      expect(completedHash).toEqual(originalHash);
    },
    10 * 60 * 1000
  );
});
