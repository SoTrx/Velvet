import { injectable } from "inversify";
import { DataService } from "../../@types/data-service";
import {
  Campaign,
  Font,
  GMCampaign,
  Player,
  PlayerCampaign,
  Session,
} from "../../@types/api";
import * as campaigns from "./data/campaigns.json";
import * as gmCampaigns from "./data/gm-campaigns.json";
import * as players from "./data/players.json";
import * as sessions from "./data/sessions.json";
import * as playerCampaigns from "./data/player-campaigns.json";

@injectable()
export class MockDataService implements DataService {
  fetch<T>(mockData: T[], time = 0): Promise<T[]> {
    return new Promise<T[]>((resolve) => {
      setTimeout(() => {
        resolve(mockData);
      }, time);
    });
  }

  createFont(campaignId: number, content: Buffer): Promise<Font> {
    return Promise.resolve(undefined);
  }

  updateFont(campaignId: number, content: Buffer): Promise<void> {
    return Promise.resolve(undefined);
  }

  async getCampaigns(): Promise<Campaign[]> {
    return this.fetch<Campaign>(campaigns, 1000);
  }

  async getGmCampaigns(): Promise<GMCampaign[]> {
    return this.fetch<GMCampaign>(gmCampaigns, 1000);
  }

  async getPlayers(): Promise<Player[]> {
    return this.fetch<Player>(players, 1000);
  }

  async getPlayersCampaign(): Promise<PlayerCampaign[]> {
    return this.fetch<PlayerCampaign>(playerCampaigns, 1000);
  }

  getFont(id: number): Promise<Font> {
    return Promise.resolve(undefined);
  }

  getFonts(): Promise<Font[]> {
    return Promise.resolve([]);
  }

  async getSessions(): Promise<Session[]> {
    return this.fetch<Session>(sessions, 1000);
  }

  async getCampaignsForUser(userId: string): Promise<Campaign[]> {
    const playerIds = (await this.getPlayers())
      .filter((p) => p.idDiscord !== null && p.idDiscord?.toString() === userId)
      .map((p) => p.idRoll20.toString());

    const gmCampaigsIds = (await this.getGmCampaigns())
      .filter((gmC) => playerIds.includes(gmC.roll20Id))
      .map((gmC) => gmC.campaignId.toString());
    return (await this.getCampaigns()).filter((c) =>
      gmCampaigsIds.includes(c.id.toString())
    );
  }

  async getSessionsForCampaign(campaignId: string): Promise<Session[]> {
    return (await this.getSessions())?.filter(
      (s) => s.campaignid.toString() === campaignId.toString()
    );
  }

  async getGmsOfCampaign(campaignId: string): Promise<Player[]> {
    const gmIds = (await this.getGmCampaigns())
      .filter((gmC) => campaignId == gmC.campaignId)
      .map((gmC) => gmC.roll20Id.toString());
    return (await this.getPlayers()).filter((p) =>
      gmIds.includes(String(p.idRoll20))
    );
  }

  async addSession(session: Session): Promise<Session> {
    return session;
  }

  async getPlayersOfCampaign(campaignId: string): Promise<Player[]> {
    const playerIds = (await this.getPlayersCampaign())
      .filter((pC) => campaignId == pC.CampaignId)
      .map((pC) => pC.PlayerIdRoll20.toString());
    return (await this.getPlayers()).filter((p) =>
      playerIds.includes(String(p.idRoll20))
    );
  }

  async createCampaign(campaign: Campaign): Promise<Campaign> {
    return Promise.resolve(campaign);
  }

  updatePlayersOfCampaign(
    campaignId: string,
    players: Partial<Player>[]
  ): Promise<void> {
    return Promise.resolve(undefined);
  }

  patchCampaignById(
    campaignId: string,
    patch: Partial<Campaign>
  ): Promise<void> {
    return Promise.resolve();
  }
}
