import "reflect-metadata";
import { ScrapperServiceAPI } from "../@types/scrapper-service";
import { ScrapperApiClient } from "./scrapper-api-client";
// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv-safe").config();
describe("Scrapper API Client", () => {
  const scrapper: ScrapperServiceAPI = new ScrapperApiClient(
    process.env.SCRAPPER_SERVER_URL
  );
  it("Should retrieve players", async () => {
    const players = await scrapper.retrievePlayersOfCampaign(6304414);
    console.log(players);
  });
  it("Should join game", async () => {
    const res = await scrapper.joinGame(
      "https://app.roll20.net/join/9507637/V_j6LQ"
    );
    console.log(res);
  });
});
