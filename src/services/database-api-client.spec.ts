import "reflect-metadata";
import { DatabaseApiClient } from "./database-api-client";
import {
  Campaign,
  Font,
  GMCampaign,
  Player,
  PlayerCampaign,
  Session,
} from "../@types/api";
import { readFileSync } from "fs";
import { resolve } from "path";
import { createHash } from "crypto";
import { DataService } from "../@types/data-service";
// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dotenv-safe").config();

describe("API-client", () => {
  const client = new DatabaseApiClient({
    dataBaseUrl: process.env.DATA_SERVER_URL,
  });
  describe("Campaigns", () => {
    it("Should retrieve all campaigns", async () => {
      const cp: Campaign[] = await client.getCampaigns();
      expect(cp).not.toBeNull();
      expect(cp.length).not.toBe(0);
    });
  });
  describe("Players", () => {
    it("Should retrieve all Players", async () => {
      const players: Player[] = await client.getPlayers();
      expect(players).not.toBeNull();
      expect(players.length).not.toBe(0);
    });
    it("Should retrieve all bindings between Gms and a Campaign", async () => {
      const gmCampaigns: GMCampaign[] = await client.getGmCampaigns();
      expect(gmCampaigns).not.toBeNull();
      expect(gmCampaigns.length).not.toBe(0);
    });
    it("Should retrieve all bindings between Players and a Campaign", async () => {
      const playerCampaigns: PlayerCampaign[] = await client.getPlayersCampaign();
      expect(playerCampaigns).not.toBeNull();
      expect(playerCampaigns.length).not.toBe(0);
    });
    it("Should get the players of a campaign", async () => {
      const players: Player[] = await client.getPlayersOfCampaign("15");
      console.log(players);
      expect(players).not.toBeNull();
      expect(players.length).not.toBe(0);
    });
    it("Should update the players of a campaign", async () => {
      const players = [
        {
          avatarUrl:
            "https://s3.amazonaws.com/files.d20.io/images/54218763/Nt2WTk5aTGNQj6KsXmifqQ/thumb.jpg?1526723804238",
          isGm: false,
          roll20Id: "2970803",
          username: "Velvet",
        },
        {
          avatarUrl:
            "https://secure.gravatar.com/avatar/27bcaf565eb011c933554195dcde6e5f?d=identicon",
          isGm: false,
          roll20Id: "1706250",
          username: "magic e.",
        },
        {
          avatarUrl:
            "https://secure.gravatar.com/avatar/52933fbf025510a0edb2b736c1a8305d?d=identicon",
          isGm: true,
          roll20Id: "946445",
          username: "SoTrx d.",
        },
      ];

      await client.updatePlayersOfCampaign("19", players);
      console.log(players);
      expect(players).not.toBeNull();
      expect(players.length).not.toBe(0);
    });
  });
  describe("Sessions", () => {
    const EXAMPLE_SESSION: Session = {
      video: `https://www.youtube.com/watch?v=MIAOU`,
      name: "Example de test unitaire",
      campaignid: "6",
      date: new Date().toISOString(),
      createdAt: new Date().toISOString(),
      index: "18",
      duration: "4544645",
    };

    it("Should retrieve all Sessions", async () => {
      const sessions: Session[] = await client.getSessions();
      console.log(sessions[0]);
      expect(sessions).not.toBeNull();
      expect(sessions.length).not.toBe(0);
    });
    it("Should be able to add a Session to a campaign", async () => {
      try {
        await client.addSession(EXAMPLE_SESSION);
      } catch (e) {
        console.error(e.response?.data);
        console.error(e.response?.data?.error?.details);
        throw e;
      }
    });
  });
  describe("Fonts", () => {
    const fontfile = readFileSync(
      resolve(__dirname, "../assets/fonts/Odinson-224w.ttf")
    );
    const campaignId = 8;
    it("Should create or update a font", async () => {
      let font;
      try {
        font = await client.getFont(campaignId);
      } catch (e) {
        //404
      }
      try {
        if (font) {
          await client.updateFont(campaignId, fontfile);
        } else {
          await client.createFont(campaignId, fontfile);
        }
      } catch (e) {
        console.error(e);
      }
    }, 180000);
    it("Should be able to retrieve a font without any changes in the data", async () => {
      let font: Font;
      try {
        font = await client.getFont(campaignId);
      } catch (e) {
        //404
      }
      const newData = createHash("md5").update(font.fontfile).digest("hex");
      const originalData = createHash("md5").update(fontfile).digest("hex");

      expect(newData).toEqual(originalData);
    }, 180000);
  });
});
