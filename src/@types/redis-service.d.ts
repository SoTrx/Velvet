import { RedisMessage } from "./redis-message";
import EventEmitter = NodeJS.EventEmitter;

export interface RedisAPI extends EventEmitter {
  publish(channel: string, payload: Record<string, any>);

  subscribe(channel: string);

  unsubscribe(channel: string): Promise<void>;

  waitFor(channel: string): Promise<RedisMessage>;
}
