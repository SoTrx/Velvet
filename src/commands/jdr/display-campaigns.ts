import { Message, MessageEmbed } from "discord.js";
import { DataCommand } from "../../components/data-command";
import { chooseOneItem } from "../../utilities/user-interaction";
import { Campaign } from "../../@types/api";
import { CommandoClient } from "discord.js-commando";
import { secondsToDhms } from "../../utilities/duration-converter";

export default class DisplayCampaigns extends DataCommand {
  private readonly MAX_EPISODES_TO_SHOW = 3;

  constructor(client) {
    super(client, {
      name: "chroniques",
      aliases: ["rc"],
      memberName: "chroniques",
      group: "jdr",
      description: "Affiche toutes les chroniques",
    });
  }

  async getCampaigns(): Promise<Campaign[]> {
    return this.dataApi.getCampaigns();
  }

  async run(message): Promise<Message> {
    const campaigns = await this.getCampaigns();
    const question = "De quelle chronique voir les détails ?";
    campaigns.forEach((c) => {
      if (c.isFinished) c.name += " **[TERMINÉE]**";
    });
    const chosenCampaign = await chooseOneItem<Campaign, string>(
      message,
      campaigns,
      "name",
      question
    );
    if (chosenCampaign === undefined) return;
    const embed = await this.toEmbedData(chosenCampaign, message.client);
    return message.embed(embed);
  }

  async toEmbedData(
    campaign: Campaign,
    client: CommandoClient
  ): Promise<MessageEmbed> {
    const embed = new MessageEmbed();
    const sessions = await this.dataApi.getSessionsForCampaign(
      campaign?.id.toString()
    );
    const GMs = await this.dataApi.getGmsOfCampaign(String(campaign.id));
    const duration = sessions
      .map((s) => Number(s.duration))
      .reduce((acc: number, duration: number) => acc + duration, 0);
    embed.addField("*Durée totale de jeu*", secondsToDhms(duration));

    // Display players of campaign
    const players = await this.dataApi.getPlayersOfCampaign(
      String(campaign.id)
    );
    if (players.length !== 0) {
      const playersNames = players
        .map(
          (p) => p.username.charAt(0).toUpperCase() + p.username.substring(1)
        )
        .sort()
        .join(", ");
      embed.addField("Joueurs", `${playersNames}`);
    }

    //Episodes && Playlist
    embed.addField("Playlist", campaign.playlist);
    embed.addField(
      "Est une campagne réservée aux One Shots",
      campaign.isForOneShots ? "Oui" : "Non"
    );
    embed.addField("\u200b", "**Derniers épisodes:**");
    sessions
      .slice(-this.MAX_EPISODES_TO_SHOW)
      .reverse()
      .forEach((s) => {
        embed.addField(s.name, s.video || "En cours de tournage");
      });

    embed.setColor("#a0b448");
    embed.setDescription(
      campaign.description || "Une super chronique comme TPMP"
    );
    embed.setFooter(client.user?.username, client.user?.displayAvatarURL());
    const gmNames = GMs.map(
      (gm) => gm.username.charAt(0).toUpperCase() + gm.username.substring(1)
    ).join(" & ");
    embed.setTitle(`${campaign.name} - (${gmNames})`);
    embed.setURL(campaign.playlist);
    embed.setThumbnail(
      campaign.image ||
        "https://preview.ibb.co/n8hGvy/angry_cat_noises_cropped.jpg"
    );
    embed.setTimestamp();

    return embed;
  }
}
