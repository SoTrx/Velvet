import "reflect-metadata";
import { Roll20Recorder } from "./roll20-recorder";
import { Campaign } from "../@types/api";
import * as ffmpeg from "fluent-ffmpeg";
import { FfmpegCommand } from "fluent-ffmpeg";
import { tmpdir } from "os";

describe("Roll20 recorder", () => {
  const campaign: Campaign = {
    id: 99,
    roll20Link: "https://app.roll20.net/join/2883710/mEiLZw",
    image: undefined,
    description: "...",
    name: "Test",
    isFinished: false,
    playlist: "...",
    createdAt: new Date().toISOString(),
  };
  const roll20Recorder = new Roll20Recorder(null);
  it("Should be able to record a roll20 Clip", async () => {
    const capturePath = `${tmpdir()}/roll20-capture-${Date.now()}.flv`;
    await roll20Recorder.startRecording(campaign);
    const ffmpegProcess = ffmpeg()
      .input(roll20Recorder.recordingPath)
      .outputFormat("flv")
      .save(capturePath);
    await new Promise((res, _) => setTimeout(() => res(), 120000));
    await roll20Recorder.stopRecording();
    await ffmpegProcess.kill("SIGKILL");
  }, 240000);
});
