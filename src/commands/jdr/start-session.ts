import { Message } from "discord.js";
import { DataCommand } from "../../components/data-command";
import { MainRecorderAPI } from "../../@types/recording-service";
import getDecorators from "inversify-inject-decorators";
import { container } from "../../inversify.config";
import { TYPES } from "../../types";
import {
  chooseOneItem,
  waitForMessage,
} from "../../utilities/user-interaction";
import { Campaign } from "../../@types/api";
import { GlobalExt } from "../../@types/global";
import { CommandoClient, CommandoMessage } from "discord.js-commando";
import { RecordingMode } from "../../services/recorder";
import CreateCampaign from "./create-campaign";
import { ScrapperServiceAPI } from "../../@types/scrapper-service";

const { lazyInject } = getDecorators(container);
declare const global: GlobalExt;
type videoRecorderFlag = "--vm" | "--roll20";
type videoRecorderOption = "vm" | "roll20";
interface Args {
  videoRecorder: "--vm" | "--roll20";
}
export default class StartSession extends DataCommand {
  @lazyInject(TYPES.RecordingService)
  private recorderInstance: MainRecorderAPI;

  @lazyInject(TYPES.ScrapperService)
  private scrapper: ScrapperServiceAPI;

  constructor(client: CommandoClient) {
    super(client, {
      name: "session",
      aliases: [],
      memberName: "session",
      group: "jdr",
      description: "Enregistre une nouvelle session",

      argsPromptLimit: 5 * 60,

      args: [
        {
          key: "videoRecorder",
          prompt: "Enregistrer sur la vm ?",
          type: "string",
          oneOf: ["--vm", "--roll20"],
          default: "--roll20",
          error: "Moyen d'enregistrement invalide",
          wait: 5 * 60,
        },
      ],
    });
  }

  async run(message, args: Args): Promise<Message> {
    if (global.recorderInstance) {
      await message.say("Un enregistrement est déjà en cours !");
      return;
    }
    const voiceChannel = (await message.guild.members.fetch(message.author.id))
      ?.voice.channel;
    if (!voiceChannel) {
      await message.say("Non connecté à un channel vocal !");
      return;
    }
    const userCampaigns = (
      await this.dataApi.getCampaignsForUser(message.author.id)
    )?.filter((c) => !c.isFinished);

    const question = "De quelle chronique tu veux enregistrer un épisode ?";

    global.recordedCampaign = await chooseOneItem<Campaign, string>(
      message,
      userCampaigns,
      "name",
      question
    );
    if (global.recordedCampaign === undefined) return;
    global.recorderInstance = this.recorderInstance;
    if (
      global.recordedCampaign.isForOneShots &&
      this.sanitizeVideoRecorder(args.videoRecorder) === "roll20"
    ) {
      await message.say(
        "Cette chronique est faite pour les One Shots ! Quel est le lien pour rejoindre le jeu sur Roll20 ?"
      );

      const linkMessage = await waitForMessage(
        this.client,
        message.author.id,
        (content: string, message: CommandoMessage) => {
          const valid = CreateCampaign.ROLL20_JOIN_LINK_MATCHER.test(content);
          if (!valid)
            message.channel.send("Ce lien Roll20 n'est pas valide ! Retente.");
          return valid;
        },
        -1
      );
      await this.scrapper.joinGame(linkMessage.content);
      // Temporarily overriding default link
      global.recordedCampaign.roll20Link = linkMessage.content;
    }
    await message.say(
      "Contact des processus d'enregistement vidéo...\n Cela peut prendre jusqu'à 2 minutes"
    );
    await global.recorderInstance.startRecording(
      voiceChannel,
      global.recordedCampaign,
      this.sanitizeVideoRecorder(args.videoRecorder)
    );

    if (global.recorderInstance.recordingMode === RecordingMode.VideoAndAudio) {
      await message.say(
        `Succès du contact ! Mode d'enregistrement choisi : Audio et vidéo !`
      );
    } else if (
      global.recorderInstance.recordingMode === RecordingMode.AudioOnly
    ) {
      await message.say(
        `Échec du contact ! Mode d'enregistrement choisi : Audio seulement !`
      );
    }

    return message.say("Enregistrement démarré!");
  }

  private sanitizeVideoRecorder(flag: videoRecorderFlag): videoRecorderOption {
    return <videoRecorderOption>flag.replace("--", "").trim();
  }
}
