import { Client, GuildMember, Message } from "discord.js";
import { DataCommand } from "../../components/data-command";
import { Campaign } from "../../@types/api";
import { CommandoClient, CommandoMessage } from "discord.js-commando";
import {
  chooseOneItem,
  waitForMentionedUsers,
} from "../../utilities/user-interaction";

export default class AddGm extends DataCommand {
  constructor(client: CommandoClient) {
    super(client, {
      name: "ajoutermj",
      aliases: ["addgm"],
      memberName: "ajoutermj",
      group: "jdr",
      description: "Ajoute un MJ à une chronique",
      examples: [`$ajoutermj`],
      argsPromptLimit: 5 * 60,
    });
  }

  async run(message, args: any): Promise<Message> {
    const userCampaigns = await this.dataApi.getCampaignsForUser(
      message.author.id
    );
    const question = "À quelle chronique ajouter un MJ ?";
    const chosenCampaign = await chooseOneItem<Campaign, string>(
      message,
      userCampaigns,
      "name",
      question
    );
    await message.channel.send(
      `Qui ajouter en tant que MJ dans la chronique ${chosenCampaign.name} ? (Mentionner le/les membres)`
    );
    const members = await waitForMentionedUsers(
      this.client,
      message,
      message.author.id
    );
    const players = await this.dataApi.getPlayers();
    const errored: GuildMember[] = [];
    const playersIds = members.map((member) => {
      const roll20Ids = players.find(
        (p) => String(p.idDiscord) === String(member.id)
      )?.idRoll20;
      if (roll20Ids === undefined) errored.push(member);
      return {
        roll20Id: Number(roll20Ids),
        isGm: true,
        avatarUrl: member.user.avatarURL(),
        username: member.user.username,
      };
    });
    try {
      await this.dataApi.updatePlayersOfCampaign(
        String(chosenCampaign.id),
        playersIds
      );
      await message.channel.send("Mj(s) ajouté(s) !");
    } catch (e) {
      console.error(e.message);
      throw e;
    }

    return message;
  }
}
