import { Client, Message } from "discord.js";
import { DataCommand } from "../../components/data-command";
import { Campaign } from "../../@types/api";
import { TYPES } from "../../types";
import { YoutubeAPI } from "../../@types/youtube-service";
import getDecorators from "inversify-inject-decorators";
import { container } from "../../inversify.config";
import { YoutubeQuotaError } from "../../services/youtube-service";
import { youtube_v3 } from "googleapis";
import { RedisAPI } from "../../@types/redis-service";
import axios from "axios";
import { ScrapperServiceAPI } from "../../@types/scrapper-service";
import { PubChannels } from "../../services/redis-service";
import { CommandoClient, CommandoMessage } from "discord.js-commando";

const { lazyInject } = getDecorators(container);

interface Args {
  name: string;
  link: string;
  description: string;
  image?: string;
  isforoneshots: boolean;
}

export default class CreateCampaign extends DataCommand {
  private static readonly DEFAULT_CAMPAIGN_IMAGE =
    "https://res.cloudinary.com/datfhmsze/image/upload/v1585769659/vel_cropped_y1qyzk.jpg";
  public static readonly ROLL20_JOIN_LINK_MATCHER = /^(.{1,})app\.roll20\.net\/join\/\d{5,}\/(.*)$/;
  private static readonly DEFAULT_ARG_IMAGE = "non";
  @lazyInject(TYPES.YoutubeService)
  private youtube: YoutubeAPI;

  @lazyInject(TYPES.ScrapperService)
  private scrapper: ScrapperServiceAPI;

  constructor(client: CommandoClient) {
    super(client, {
      name: "nouvellechronique",
      aliases: ["cc"],
      memberName: "nouvellechronique",
      group: "jdr",
      description: "Crée une nouvelle chronique prête à être enregistrée",
      examples: [
        `$nouvellechronique Ma super chronique de ouf guedin https://app.roll20.net/join/2883710/mEiLZw\
        Alors c'est l'histoire d'un pangolin d'Austral ie qui part manger du foin en Tasmanie, et c'est passionant j'vous jure !`,
      ],
      argsPromptLimit: 5 * 60,

      args: [
        {
          key: "name",
          prompt: "Nom de la chronique",
          type: "string",
          validate: (text) => text.length < 200,
          error: "Nom de chronique trop long",
          wait: 5 * 60,
        },
        {
          key: "link",
          prompt: "Lien pour rejoindre la chronique sur roll20",
          type: "string",
          validate: (text) =>
            CreateCampaign.ROLL20_JOIN_LINK_MATCHER.test(text),
          error:
            "Lien invalide, le lien correct est celui à donner aux joueurs pour rejoindre la partie !",
          wait: 5 * 60,
        },
        {
          key: "description",
          prompt: "Description de la chronique",
          type: "string",
          wait: 5 * 60,
        },
        {
          key: "isforoneshots",
          prompt:
            "Cette chronique est-elle faite pour des one-shots ? (yes/no)",
          type: "boolean",
          wait: 5 * 60,
        },
        {
          key: "image",
          prompt: `Lien vers une image décrivant la chronique. (Ecrire "${CreateCampaign.DEFAULT_ARG_IMAGE}" (sans les guillemets) pour utiliser l\'image par défaut)`,
          type: "string",
          validate: (url) => CreateCampaign.checkImageURL(url),
          error: "Ce n'est pas un lien d'image valide !",
          wait: 5 * 60,
        },
      ],
    });
  }

  static async checkImageURL(url: string): Promise<boolean> {
    if (url === CreateCampaign.DEFAULT_ARG_IMAGE) return true;
    if (!url?.startsWith("http")) return false;
    try {
      const headers = (
        await axios.head(url, {
          responseType: "stream",
        })
      ).headers;
      const cType: string = headers["content-type"];
      return cType.startsWith("image");
    } catch (e) {
      return false;
    }
  }

  async run(message, args: Args): Promise<Message> {
    if (!args.image || args.image === CreateCampaign.DEFAULT_ARG_IMAGE) {
      args.image = CreateCampaign.DEFAULT_CAMPAIGN_IMAGE;
    }
    if (!args.image.startsWith("http")) {
      await message.say(
        `Image non valide : "${args.image}". Si la description a débordé sur l'image, il faut utiliser des guillemets pour la description. Annulation de la commande !`
      );
      return;
    }
    let playlist: youtube_v3.Schema$Playlist;
    try {
      playlist = await this.youtube.createPlaylist({
        description: args.description,
        title: args.name,
      });
    } catch (e) {
      if (e instanceof YoutubeQuotaError) {
        await message.say(
          "Le Quota Youtube est épuisé pour aujourd'hui ! La playlist de la chronique ne peut pas être crée ! Réessaye demain !"
        );
        return;
      }
      await message.say(
        `Impossible de créer la playlist Youtube ! Erreur (${
          e.code
        }) : ${e.errors.map((e) => `${e.domain} => ${e.reason}`).join("\n")}`
      );
      console.error(e);
      return;
    }
    let campaign: Campaign = {
      isFinished: false,
      createdAt: new Date().toISOString(),
      description: args.description,
      roll20Link: args.link,
      name: args.name,
      isForOneShots: args.isforoneshots,
      playlist: `https://www.youtube.com/playlist?list=${playlist.id}`,
      image: args.image,
    };

    try {
      campaign = await this.dataApi.createCampaign(campaign);
    } catch (e) {
      await message.say(
        "Impossible de créer la campagne, Réessayez plus tard!"
      );
      console.error(e);
      return;
    }

    await message.say("Campagne créee avec succès !");

    await message.say(
      "Synchronisation des paramètres avec Roll20 ... \n Cette étape peut prendre jusqu'à 5 minutes."
    );
    try {
      const cR20Id = campaign.roll20Link.match(/\/(\d+)/)[1];
      await this.scrapper.joinGame(campaign.roll20Link);
      const players = await this.scrapper.retrievePlayersOfCampaign(
        Number(cR20Id)
      );
      await this.dataApi.updatePlayersOfCampaign(String(campaign.id), players);
    } catch (e) {
      await message.say(`Synchronisation échouée  ! Erreur : ${e.message}`);
      return;
    }
    await message.say("Synchronisation terminée ! Campagne crée avec succès");

    return message;
  }
}
