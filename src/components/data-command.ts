import { Command, CommandInfo, CommandoClient } from "discord.js-commando";
import getDecorators from "inversify-inject-decorators";
import { container } from "../inversify.config";
import { TYPES } from "../types";
import { DataService } from "../@types/data-service";

const { lazyInject } = getDecorators(container);

export abstract class DataCommand extends Command {
  @lazyInject(TYPES.DataService)
  protected dataApi: DataService;

  protected constructor(client: CommandoClient, infos: CommandInfo) {
    super(client, infos);
  }
}
