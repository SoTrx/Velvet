import { injectable } from "inversify";
import { Player } from "../@types/api";
import axios from "axios";
import { ScrapperServiceAPI } from "../@types/scrapper-service";

@injectable()
export class ScrapperApiClient implements ScrapperServiceAPI {
  private readonly apiBaseUrl;
  private readonly endpoints = {
    players: (cR20Id) => `${this.apiBaseUrl}/campaign/${cR20Id}/players`,
    join: (url) =>
      `${this.apiBaseUrl}/campaign/join/${encodeURIComponent(url)}`,
  };

  constructor(scrapperApiBaseUrl: string) {
    this.apiBaseUrl = scrapperApiBaseUrl;
  }

  async retrievePlayersOfCampaign(campaignRoll20Id: number): Promise<Player[]> {
    return (await axios.get<Player[]>(this.endpoints.players(campaignRoll20Id)))
      .data;
  }

  async joinGame(joinUrl: string): Promise<void> {
    await axios.get<Player[]>(this.endpoints.join(joinUrl));
  }
}
