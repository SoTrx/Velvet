import { Campaign, Font, GMCampaign, Player, Session } from "./api";

export interface DataService {
  getCampaigns(): Promise<Campaign[]>;

  getPlayers(): Promise<Player[]>;

  getPlayersOfCampaign(campaignId: string): Promise<Player[]>;

  getGmCampaigns(): Promise<GMCampaign[]>;

  getGmCampaigns(): Promise<GMCampaign[]>;

  getSessions(): Promise<Session[]>;

  getFonts(): Promise<Font[]>;

  getFont(id: number): Promise<Font>;

  updateFont(campaignId: number, content: Buffer): Promise<void>;

  createFont(campaignId: number, content: Buffer): Promise<Font>;

  getCampaignsForUser(userId: string): Promise<Campaign[]>;

  getSessionsForCampaign(campaignId: string): Promise<Session[]>;

  getGmsOfCampaign(campaignId: string): Promise<Player[]>;

  addSession(session: Session): Promise<Session>;

  createCampaign(campaign: Campaign): Promise<Campaign>;

  updatePlayersOfCampaign(
    campaignId: string,
    players: Partial<any>[]
  ): Promise<void>;

  patchCampaignById(
    campaignId: string,
    patch: Partial<Campaign>
  ): Promise<void>;
}

export interface DataServiceOptions {
  dataBaseUrl: string;
}
