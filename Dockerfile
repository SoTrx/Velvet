FROM node:current-alpine as build
# set working directory
WORKDIR /app
# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH
# install and cache app dependencies
COPY package.json /app/package.json
RUN apk add git yarn && yarn install
# add app
COPY . /app
# generate build
RUN yarn run build
############
### prod ###
############
FROM node:current-alpine

# add the required dependencies
WORKDIR /app

COPY --from=build /app/dist /app

RUN apk add yarn \
    && yarn global add pm2 modclean \
    && apk add --no-cache --virtual .build-deps git \
    && apk add --no-cache ffmpeg \
    && yarn install --only=prod \
    && modclean -r \
    && modclean -r /usr/local/lib/node_modules/pm2 \
    && yarn global remove modclean \
    && yarn cache clean \
    && apk del .build-deps yarn \
    && rm -rf /root/.npm /usr/local/lib/node_modules/npm

CMD ["pm2-runtime", "/app/main.js"]