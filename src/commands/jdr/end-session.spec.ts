import "reflect-metadata";
import { container } from "../../inversify.config";
import { DataService } from "../../@types/data-service";
import { TYPES } from "../../types";
import { MockDataService } from "../../services/mock/mock-data-service";
import { CommandoClient } from "discord.js-commando";
import EndSession from "./end-session";
import { MockYoutubeService } from "../../services/mock/mock-youtube-service";
import { YoutubeAPI } from "../../@types/youtube-service";
import { RedisAPI } from "../../@types/redis-service";
import { MockRedisService } from "../../services/mock/mock-redis-service";
import {
  waitForMessage,
  yesNoQuestion,
} from "../../utilities/user-interaction";
import { mocked } from "ts-jest/utils";
import { Message } from "discord.js";

jest.mock("discord.js-commando");
jest.mock("../../utilities/user-interaction");

describe("displayCampaigns", () => {
  const command = new EndSession(new CommandoClient());
  beforeAll(() => {
    container.rebind<DataService>(TYPES.DataService).to(MockDataService);
    container.rebind<YoutubeAPI>(TYPES.YoutubeService).to(MockYoutubeService);
    container.rebind<RedisAPI>(TYPES.RedisService).to(MockRedisService);
  });

  describe("Should only let GMs end sessions", () => {
    it("Only 1 Gm, authorized", async () => {
      const res = await command.checkGms("8", "287932779352555520");
      expect(res).toEqual(true);
    });
    it("Only 1 GM, not authorized", async () => {
      const res = await command.checkGms("8", "152781737863151616");
      expect(res).toEqual(false);
    });
    it("Multiple GMs, all authorized", async () => {
      const gm1 = await command.checkGms("14", "152844030097620992");
      expect(gm1).toEqual(true);
      const gm2 = await command.checkGms("14", "188626510901542912");
      expect(gm2).toEqual(true);
    });
    it("Multiple GMs, not authorized", async () => {
      const gm1 = await command.checkGms("14", "152844030097620992");
      expect(gm1).toEqual(true);
      const gm2 = await command.checkGms("14", "188998511579103232");
      expect(gm2).toEqual(false);
    });
  });
  describe("Should properly ask for the episode title", () => {
    const mockedWaitForMessage = mocked(waitForMessage, true);
    const mockedIsTitleWanted = mocked(yesNoQuestion, true);
    const SAMPLE_TITLE = "miaou";

    describe("GM wants a title", () => {
      beforeAll(() => {
        mockedIsTitleWanted.mockResolvedValue(true);
        mockedWaitForMessage.mockResolvedValue(({
          content: SAMPLE_TITLE,
        } as unknown) as Message);
      });
      it("Should return the correct title", async () => {
        const title = await command.askEpisodeTitle(null);
        expect(title).toEqual(SAMPLE_TITLE);
      });
    });
    describe("GM doesn't want a title", () => {
      beforeAll(() => {
        mockedIsTitleWanted.mockResolvedValue(false);
        mockedWaitForMessage.mockResolvedValue(({
          content: SAMPLE_TITLE,
        } as unknown) as Message);
      });
      it("Should return null", async () => {
        const title = await command.askEpisodeTitle(null);
        expect(title).toBeNull();
      });
    });
    describe("Timeout on the yes/no question", () => {
      beforeAll(() => {
        mockedIsTitleWanted.mockImplementation(() => {
          throw new Error("Timeout!");
        });
        mockedWaitForMessage.mockResolvedValue(({
          content: SAMPLE_TITLE,
        } as unknown) as Message);
      });
      it("Should return null", async () => {
        const title = await command.askEpisodeTitle(null);
        expect(title).toBeNull();
      });
    });
    describe("Timeout when asking the title", () => {
      beforeAll(() => {
        mockedIsTitleWanted.mockResolvedValue(true);
        mockedWaitForMessage.mockImplementation(() => {
          throw new Error("Timeout!");
        });
      });
      it("Should return null", async () => {
        const title = await command.askEpisodeTitle(null);
        expect(title).toBeNull();
      });
    });
  });
});
