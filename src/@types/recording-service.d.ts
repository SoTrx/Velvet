import { RecordingMode } from "../services/recorder";
import { Campaign } from "./api";
import { Readable } from "stream";
import { VoiceChannel } from "discord.js";
type AccurateTime = [number, number];
export interface RecorderAPI {
  recordingPath?: string;

  recordingMode: RecordingMode;

  startRecording(...args: any[]): void;

  stopRecording(): Promise<number | AccurateTime>;
}

export interface Roll20RecorderAPI extends RecorderAPI {
  startRecording(campaign: Campaign): void;
  stopRecording(): Promise<AccurateTime>;
}
export interface Roll20RecorderOptions {
  streamingServerUrl: string;
}

export interface DiscordRecorderAPI extends RecorderAPI {
  startRecording(voiceChannelId: string): void;
  getAudioStream(): Promise<Readable>;
  getAudioStreams(): Promise<Readable[]>;
  stopRecording(): Promise<AccurateTime>;
  recordId: string;
}

export interface DiscordRecorderOptions {
  cookingServerUrl: string;
}
export interface MainRecorderAPI extends RecorderAPI {
  on(
    event: "recorderMixingProgress",
    listener: (percentage: number) => void
  ): this;
  on(
    event: "audioDownloadProgress",
    listener: (totalSize: number) => void
  ): this;
  // eslint-disable-next-line @typescript-eslint/ban-types
  on(event: string, listener: Function): this;
  // eslint-disable-next-line @typescript-eslint/ban-types
  once(event: string, listener: Function): this;
  removeAllListeners(event: string): void;
  processRecording(): Promise<string>;
  stopRecording(): Promise<number>;
  calculateOffset(video: AccurateTime, audio: AccurateTime): number;
  startRecording(
    voiceChannel: VoiceChannel,
    campaign: Campaign,
    videoRecorder: "vm" | "roll20"
  ): void;
}
