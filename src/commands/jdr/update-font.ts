import { Message, MessageAttachment } from "discord.js";
import { DataCommand } from "../../components/data-command";
import { Campaign, Font } from "../../@types/api";
import { TYPES } from "../../types";
import { YoutubeAPI } from "../../@types/youtube-service";
import getDecorators from "inversify-inject-decorators";
import { container } from "../../inversify.config";
import {
  chooseOneItem,
  downloadAttachment,
  waitForMessage,
  yesNoQuestion,
} from "../../utilities/user-interaction";
import { ThumbnailGeneratorAPI } from "../../@types/thumbnail-generator-API";
import { CommandoMessage } from "discord.js-commando";
import { readFileSync, unlinkSync } from "fs";

const { lazyInject } = getDecorators(container);

interface Args {
  bypass: boolean;
}

export default class UpdateFont extends DataCommand {
  @lazyInject(TYPES.YoutubeService)
  private youtube: YoutubeAPI;
  @lazyInject(TYPES.ThumbnailService)
  private thumbnailGenerator: ThumbnailGeneratorAPI;

  constructor(client) {
    super(client, {
      name: "changerpolice",
      aliases: ["changefont"],
      memberName: "changerpolice",
      group: "jdr",
      description:
        "Change la police des miniatures (futures) d'une chronique !",
      examples: ["$changerPolice"],
      args: [
        {
          key: "bypass",
          prompt: "Bypass des droits de chroniques",
          type: "boolean",
          default: false,
        },
      ],
    });
  }

  async run(message, args: Args): Promise<Message> {
    let userCampaigns = await this.dataApi.getCampaignsForUser(
      message.author.id
    );
    if (args.bypass) {
      if (!this.client.isOwner(message.author)) {
        await message.say("Pas de bypass possible pour les non-admins !");
        return;
      }
      userCampaigns = await this.dataApi.getCampaigns();
    }

    const question = "De quelle chronique changer la police des miniatures ?";

    const chosenCampaign = await chooseOneItem<Campaign, string>(
      message,
      userCampaigns,
      "name",
      question
    );

    if (!chosenCampaign) return;

    let font;
    try {
      font = await this.dataApi.getFont(chosenCampaign.id);
    } catch (e) {
      //
    }
    const path = await this.thumbnailGenerator.generateSampleThumbnailForCampaign(
      chosenCampaign.id,
      "Un très gros exemple !"
    );

    await message.say(
      "Voilà un exemple de miniature avec la police actuelle :",
      {
        files: [path],
      }
    );

    const answer = await yesNoQuestion(
      message,
      message.author.id,
      "Tu veux changer la police ? ",
      3 * 60 * 1000
    );
    if (answer === null) {
      await message.say("Trop lent ! Je prends ça pour un non");
      return;
    } else if (answer === false) {
      await message.say("Ok ok");
      return;
    }
    await message.say(
      "Upload le .ttf de la nouvelle police dans le chat... (Je te laisse 10 minutes)"
    );

    const fontMessage = await waitForMessage(
      message.client,
      message.author.id,
      (messageContent: string, message: CommandoMessage) => {
        if (message.attachments?.array().length > 0) {
          if (!message.attachments.first().name.endsWith(".ttf")) {
            message.say("Le fichier doit être au format .ttf ! A refaire !");
            return false;
          }
          return true;
        }
        return false;
      },
      10 * 60 * 1000
    );
    await this.upsertFontFromAttachment(
      fontMessage.attachments.first(),
      chosenCampaign,
      font
    );
    const path2 = await this.thumbnailGenerator.generateSampleThumbnailForCampaign(
      chosenCampaign.id,
      "Un très gros exemple !"
    );

    await message.say(
      "Voilà un exemple de miniature avec la nouvelle police :",
      {
        files: [path2],
      }
    );

    const confirmFontChange = await yesNoQuestion(
      message,
      message.author.id,
      "Valider le changement de police ? ",
      3 * 60 * 1000
    );

    if (!confirmFontChange) {
      try {
        await message.say("OK on annule les changements");
        if (font)
          await this.dataApi.updateFont(chosenCampaign.id, font.fontfile);
        await message.say("Changements annulés");
      } catch (e) {
        console.error(e.message);
      }
    } else {
      const textFormatter = (progressPercentage) =>
        `Changement des anciennes miniatures de la chroniques en cours : ${progressPercentage}%`;
      const thumbnailMessage: Message = await message.say(textFormatter(0));

      await this.changeOldThumbnails(chosenCampaign, (progress: number) => {
        thumbnailMessage.edit(textFormatter(progress.toFixed(2)));
      });
      await message.say(
        "Anciennes miniatures changées, les futures miniatures prendront également en compte la nouvele police !"
      );
    }

    return message;
  }

  private async changeOldThumbnails(
    chosenCampaign: Campaign,
    progressCallback: (progress: number) => any
  ) {
    const sessions = await this.dataApi.getSessionsForCampaign(
      String(chosenCampaign.id)
    );
    const nbSessions = sessions.length;
    let cpt = 0;
    for (const session of sessions) {
      try {
        const path = await this.thumbnailGenerator.generateThumbnailForSession(
          session.id
        );
        await this.youtube.updateThumbnail(
          this.youtube.getIdFromUrl(session.video),
          path
        );
        progressCallback((++cpt / nbSessions) * 100);
      } catch (e) {
        console.error(e);
      }
    }
  }

  private async upsertFontFromAttachment(
    fontMessage: MessageAttachment,
    chosenCampaign: Campaign,
    previousFont: Font
  ) {
    const fontPath = `/tmp/font_${Date.now()}`;
    await downloadAttachment(fontMessage, fontPath);
    const fileBuffer = readFileSync(fontPath);
    try {
      if (previousFont) {
        await this.dataApi.updateFont(chosenCampaign.id, fileBuffer);
      } else {
        await this.dataApi.createFont(chosenCampaign.id, fileBuffer);
      }
    } catch (e) {
      console.error(e.message);
    }
  }
}
