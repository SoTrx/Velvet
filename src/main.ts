import "reflect-metadata";
import { container } from "./inversify.config";
import { TYPES } from "./types";
import { Velvet } from "./Velvet";

const velvet = container.get<Velvet>(TYPES.Velvet);
velvet.bootUp();
