import { Client, GuildMember, Message } from "discord.js";
import { DataCommand } from "../../components/data-command";
import { Campaign } from "../../@types/api";
import { CommandoClient, CommandoMessage } from "discord.js-commando";
import {
  chooseOneItem,
  waitForMentionedUsers,
} from "../../utilities/user-interaction";

export default class CloseCampaign extends DataCommand {
  constructor(client: CommandoClient) {
    super(client, {
      name: "terminerchronique",
      aliases: ["finish", "close"],
      memberName: "terminerchronique",
      group: "jdr",
      description: "Marque la chronique comme terminée.",
      examples: [`$terminervhronique`, `$close`, `$finish`],
      argsPromptLimit: 5 * 60,
    });
  }

  async run(message, args: any): Promise<Message> {
    const userCampaigns = await this.dataApi.getCampaignsForUser(
      message.author.id
    );
    const question = "Quelle chronique faut-il marquer comme terminée ?";
    const chosenCampaign = await chooseOneItem<Campaign, string>(
      message,
      userCampaigns,
      "name",
      question
    );
    try {
      await this.dataApi.patchCampaignById(String(chosenCampaign.id), {
        isFinished: true,
      });
    } catch (e) {
      console.error(e);
      await message.channel.send(
        "Couldn't update the campaign. Reason " + e.message
      );
    }

    await message.channel.send("Chronique marquée comme terminée !");

    return message;
  }
}
