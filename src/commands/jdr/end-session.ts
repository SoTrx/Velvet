import { Message } from "discord.js";
import { DataCommand } from "../../components/data-command";
import getDecorators from "inversify-inject-decorators";
import { container } from "../../inversify.config";
import { TYPES } from "../../types";
import {
  progressBar,
  waitForMessage,
  yesNoQuestion,
} from "../../utilities/user-interaction";
import { Session } from "../../@types/api";
import { GlobalExt } from "../../@types/global";
import { UploadProgress, YoutubeAPI } from "../../@types/youtube-service";
import { CommandoClient } from "discord.js-commando";
import { RedisAPI } from "../../@types/redis-service";
import { ThumbnailGeneratorAPI } from "../../@types/thumbnail-generator-API";

const { lazyInject } = getDecorators(container);
declare const global: GlobalExt;

export default class EndSession extends DataCommand {
  @lazyInject(TYPES.YoutubeService)
  private youtube: YoutubeAPI;
  @lazyInject(TYPES.ThumbnailService)
  private thumbnailGenerator: ThumbnailGeneratorAPI;
  @lazyInject(TYPES.RedisService)
  private redis: RedisAPI;
  // Episode title question timeout in ms
  private static readonly TITLE_TIMEOUT = 10 * 60 * 1000;
  // Episode title max length (100 : max yt - 12 : '- Episode x -')
  private static readonly TITLE_MAX_LENGTH = 88;

  constructor(client: CommandoClient) {
    super(client, {
      name: "fin",
      aliases: ["end"],
      memberName: "fin",
      group: "jdr",
      description: "Terminer l'enregistrement en cours",
    });
  }

  /**
   * Check if the author is a GM of the currently recording campaign
   * @param campaignId
   * @param authorId
   */
  async checkGms(campaignId: string, authorId: string): Promise<boolean> {
    const gms = await this.dataApi.getGmsOfCampaign(campaignId);
    return gms.some((gm) => gm.idDiscord === authorId);
  }

  async run(message): Promise<Message> {
    if (!global.recorderInstance) {
      await message.say("Pas d'enregistrement en cours !");
      return;
    }
    const campaignId = global.recordedCampaign.id.toString();
    if (!(await this.checkGms(campaignId, message.author.id))) {
      await message.say(
        "Seul un des maîtres du jeu peut arrêter un enregistrement !"
      );
      return;
    }

    const recordingTime = await global.recorderInstance.stopRecording();

    await message.say("Enregistrement terminé !");
    const processMessage: Message = await message.say(
      "Début du post-processing..."
    );
    global.recorderInstance.on("audioDownloadProgress", (totalSize) => {
      const sizeInMb = (totalSize / 1024 ** 2).toFixed(2);
      processMessage.edit(
        `Téléchargement de la piste audio : ${sizeInMb}MB récupérés`
      );
    });
    global.recorderInstance.on("audioConcatProgress", (progress) => {
      const pg = progressBar(progress, 100, 30);
      processMessage.edit(`Concatenation de tous les enregistrements : ${pg}`);
    });
    global.recorderInstance.on("recorderMixingProgress", (progress) => {
      const pg = progressBar(progress, 100, 30);
      processMessage.edit(`Mixage audio et vidéo : ${pg}`);
    });

    const processPromise = global.recorderInstance.processRecording();

    const sessionName = await this.askEpisodeTitle(message);
    const sessions = await this.dataApi.getSessionsForCampaign(campaignId);
    let title = `${global.recordedCampaign.name} - Episode ${
      sessions.length + 1
    }`;
    if (sessionName !== null) {
      title += ` - ${sessionName}`;
    }

    try {
      await processPromise;
      global.recorderInstance.removeAllListeners("recorderMixingProgress");
      global.recorderInstance.removeAllListeners("audioConcatProgress");
      global.recorderInstance.removeAllListeners("audioDownloadProgress");
      const pg = progressBar(100, 100, 30);
      processMessage
        .edit(`Mixage audio et vidéo : ${pg}%`)
        .catch(console.error);
    } catch (e) {
      await processMessage.edit(
        `Erreur durant le post-processing ! Va falloir le faire à la main !`
      );
      global.recorderInstance = undefined;
      global.recordedCampaign = null;
    }

    await processMessage.edit("Upload de la video sur Youtube: ");
    let video = undefined;
    try {
      const pg = (await message.say(progressBar(0, 100, 50))) as Message;
      this.youtube.on("uploadProgress", async (progress: UploadProgress) => {
        try {
          await pg.edit(progressBar(progress.current, progress.max, 50));
        } catch (e) {
          console.error(e);
        }
      });
      video = await this.youtube.uploadVideo(
        global.recorderInstance.recordingPath,
        {
          title: title,
          description: "Personne ne lit ça de toute façon",
          privacyStatus: "unlisted",
        }
      );
      await pg.edit(progressBar(100, 100, 50));
    } catch (e) {
      await message.say(
        `La video n'a pas pu être uploadée ! Satrox devra le faire à la main (path : ${global.recorderInstance.recordingPath})`
      );
      await message.say(`Raison : ${e.message}`);
      console.error(e);
      global.recorderInstance = undefined;
      global.recordedCampaign = null;

      return;
    }

    let session: Session = {
      video: `https://www.youtube.com/watch?v=${video.id}`,
      name: title,
      campaignid: global.recordedCampaign.id.toString(),
      date: new Date().toISOString(),
      createdAt: new Date().toISOString(),
      index: sessions.length.toString(),
      duration: Number(recordingTime),
    };

    // Uploader session
    session = await this.dataApi.addSession(session);

    const playlistId = this.youtube.getIdFromUrl(
      global.recordedCampaign.playlist
    );
    await this.youtube.addVideoToPlaylist(video.id, playlistId);
    await message.say("Génération de la miniature....");
    try {
      const path = await this.thumbnailGenerator.generateThumbnailForSession(
        session.id
      );
      await this.youtube.updateThumbnail(video.id, path);
      await message.say("Miniature OK !");
    } catch (e) {
      console.error(e);
      await message.say(
        "Échec de la génération ou de l'upload de la miniature, on fera sans !"
      );
    }

    await message.say(
      `Record prêt : https://www.youtube.com/watch?v=${video.id}`
    );
    global.recorderInstance = null;
    global.recordedCampaign = null;
  }

  async askEpisodeTitle(evt): Promise<string> {
    let isNamed;
    try {
      isNamed = await yesNoQuestion(
        evt,
        evt?.author?.id,
        `Donner un nom à l'épisode ?`
      );
    } catch (e) {
      evt?.reply("Trop lent, on garde le nom par défaut");
      isNamed = false;
    }
    if (!isNamed) {
      return null;
    }
    evt?.channel?.send("Quel nom donner à l'épisode ?");
    let temp;
    try {
      temp = await waitForMessage(
        evt?.client,
        evt?.author?.id,
        (message) => {
          const isAValidTitle =
            message.length + global.recordedCampaign.name.length <
            EndSession.TITLE_MAX_LENGTH;
          if (!isAValidTitle) {
            evt.channel.send(
              `C'est un titre, pas une étude de texte... Moins de ${EndSession.TITLE_MAX_LENGTH} caractères ça devrait le faire...`
            );
          }
          return isAValidTitle;
        },
        EndSession.TITLE_TIMEOUT
      );
    } catch (e) {
      evt?.reply("Trop lent, on garde le nom par défaut");
      return null;
    }
    return temp?.content;
  }
}
