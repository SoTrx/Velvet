export function allWithProgress<T>(
  proms: Promise<T>[],
  progressCallback: (progress: number) => any
): Promise<T[]> {
  let d = 0;
  progressCallback(0);
  for (const p of proms) {
    p.then(() => {
      d++;
      progressCallback((d * 100) / proms.length);
    });
  }
  return Promise.all(proms);
}
