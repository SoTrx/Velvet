export interface Campaign {
  id?: number;
  name: string;
  image?: string;
  playlist: string;
  roll20Link: string;
  description: string;
  overlayId?: number | null;
  isFinished: boolean;
  isForOneShots: boolean;
  createdAt: string;
}

export interface Player {
  idDiscord: number | string | null;
  username: string;
  idRoll20: number;
  avatarURL: string;
}

export interface Font {
  campaignid: number;
  fontfile: Buffer;
}

export interface Character {
  avatarUrl: string;
  name: string;
  id: string;
  sheet: string[] | string | null;
  isPc: boolean;
}

export interface CharacterRecord {
  id?: number;
  CampaignId: number;
  PlayerId: number;
  CharacterId: string;
}

export interface PlayerCampaign {
  id?: number;
  CampaignId: string;
  PlayerIdRoll20: string;
}

export interface GMCampaign {
  id?: number;
  campaignId: string;
  roll20Id: string;
}

export interface Roll {
  id?: number | null;
  result: number;
  campaignId: number;
  diceType: string;
  playerId: string;
  timestamp: string | null;
}

export interface Session {
  id?: number;
  name: string;
  video: string | null;
  description?: string | null;
  date: string;
  campaignid: string | number;
  index: string | number;
  duration: string | number | null;
  createdAt: string;
}
