/**
 * @description A module for parsing ISO8601 durations
 * @author https://github.com/tolu/ISO8601-duration
 */

/**
 * The pattern used for parsing ISO8601 duration (PnYnMnDTnHnMnS).
 * This does not cover the week format PnW.
 */

// PnYnMnDTnHnMnS
const numbers = "\\d+(?:[\\.,]\\d{0,3})?";
const weekPattern = `(${numbers}W)`;
const datePattern = `(${numbers}Y)?(${numbers}M)?(${numbers}D)?`;
const timePattern = `T(${numbers}H)?(${numbers}M)?(${numbers}S)?`;

const iso8601 = `P(?:${weekPattern}|${datePattern}(?:${timePattern})?)`;
const objMap = [
  "weeks",
  "years",
  "months",
  "days",
  "hours",
  "minutes",
  "seconds",
];
const pattern = new RegExp(iso8601);

/**
 * The ISO8601 regex for matching / testing durations
 */

interface DurationObject {
  weeks: number;
  years: number;
  months: number;
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
}

/** Parse PnYnMnDTnHnMnS format to object
 * @param  durationString - PnYnMnDTnHnMnS formatted string
 * @return With a property for each part of the pattern
 */
export const parse = (durationString: string): DurationObject => {
  // Slice away first entry in match-array
  return durationString
    .match(pattern)
    .slice(1)
    .reduce((prev, next, idx) => {
      prev[objMap[idx]] = parseFloat(next) || 0;
      return prev;
    }, {}) as DurationObject;
};

/**
 * Convert ISO8601 duration object to an end Date.
 *
 * @param  duration - The duration object
 * @param startDate - The starting Date for calculating the duration
 * @return  The resulting end Date
 */
export const end = (duration: DurationObject, startDate: Date): Date => {
  // Create two equal timestamps, add duration to 'then' and return time difference
  const timestamp = startDate ? startDate.getTime() : Date.now();
  const then = new Date(timestamp);

  then.setFullYear(then.getFullYear() + duration.years);
  then.setMonth(then.getMonth() + duration.months);
  then.setDate(then.getDate() + duration.days);
  then.setHours(then.getHours() + duration.hours);
  then.setMinutes(then.getMinutes() + duration.minutes);
  // Then.setSeconds(then.getSeconds() + duration.seconds);
  then.setMilliseconds(then.getMilliseconds() + duration.seconds * 1000);
  // Special case weeks
  then.setDate(then.getDate() + duration.weeks * 7);

  return then;
};

/**
 * Convert ISO8601 duration object to seconds
 *
 * @param {Object} duration - The duration object
 * @param {Date} startDate - The starting point for calculating the duration
 * @return {Number}
 */
export const toSeconds = (
  duration: DurationObject,
  startDate?: Date
): number => {
  const timestamp = startDate ? startDate.getTime() : Date.now();
  const now = new Date(timestamp);
  const then = end(duration, now);

  return (then.getTime() - now.getTime()) / 1000;
};

export const secondsToDhms = (seconds: number): string => {
  seconds = Number(seconds);
  const d = Math.floor(seconds / (3600 * 24));
  const h = Math.floor((seconds % (3600 * 24)) / 3600);
  const m = Math.floor((seconds % 3600) / 60);
  const s = Math.floor((seconds % 3600) % 60);

  const dDisplay = d > 0 ? d + (d == 1 ? " jour, " : " jours, ") : "";
  const hDisplay = h > 0 ? h + (h == 1 ? " heure, " : " heures, ") : "";
  const mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
  const sDisplay = s + (s > 1 ? " seconde" : " secondes");
  return dDisplay + hDisplay + mDisplay + sDisplay;
};
