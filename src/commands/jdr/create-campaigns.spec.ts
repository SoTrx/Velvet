import "reflect-metadata";
import { container } from "../../inversify.config";
import { DataService } from "../../@types/data-service";
import { TYPES } from "../../types";
import { MockDataService } from "../../services/mock/mock-data-service";
import { CommandoClient } from "discord.js-commando";
import CreateCampaign from "./create-campaign";
import { YoutubeQuotaError } from "../../services/youtube-service";
import { MockYoutubeService } from "../../services/mock/mock-youtube-service";
import { YoutubeAPI } from "../../@types/youtube-service";
import { Campaign } from "../../@types/api";
import { DatabaseApiClient } from "../../services/database-api-client";

jest.mock("discord.js-commando");
describe("createCampaigns", () => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const command = new CreateCampaign(CommandoClient);
  beforeAll(() => {
    container
      .rebind<YoutubeAPI>(TYPES.YoutubeService)
      .toConstantValue(new MockYoutubeService());
  });

  describe("Should validate correct roll20 Link", () => {
    beforeAll(() => {
      container.rebind<DataService>(TYPES.DataService).to(MockDataService);
    });
    it("Should validate a valid roll20 link", async () => {
      const link = "https://app.roll20.net/join/2883710/mEiLZw";
      const res = CreateCampaign.ROLL20_JOIN_LINK_MATCHER.test(link);
      expect(res).toBeTruthy();
    });
    it("Should invalidate an invalid string", async () => {
      const link = "https://app.rollds20.net/join/2883710/mEiLZw";
      const res = CreateCampaign.ROLL20_JOIN_LINK_MATCHER.test(link);
      expect(res).toBeFalsy();
    });
  });

  describe("Should validate images links", () => {
    it("Should reject invalid links", async () => {
      expect(await CreateCampaign.checkImageURL("aaaa")).toEqual(false);
      expect(
        await CreateCampaign.checkImageURL(
          "https://nodejs.org/api/stream.html#stream_event_end"
        )
      ).toEqual(false);
      expect(
        await CreateCampaign.checkImageURL(
          "https://developers.google.com/youtube/v3/docs/thumbnails"
        )
      ).toEqual(false);
      expect(
        await CreateCampaign.checkImageURL(
          "https://www.google.com/url?sa=i&url=https%3A%2F%2Fpixabay.com%2Ffr%2Fillustrations%2Fhd-fond-d-%25C3%25A9cran-arri%25C3%25A8re-plan-3816045%2F&psig=AOvVaw1m97y_5BoLMJdL-J-p1bG6&ust=1596542174155000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCJiD7Jv9_uoCFQAAAAAdAAAAABAJ"
        )
      ).toEqual(false);
    });
    it("Should accept valid links", async () => {
      expect(
        await CreateCampaign.checkImageURL(
          "https://www.gettyimages.fr/gi-resources/images/500px/983794168.jpg"
        )
      ).toEqual(true);
      expect(
        await CreateCampaign.checkImageURL(
          "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT8Qxs4YcEguE11ZpSAArIaNFrpyp7JbtWA9A&usqp=CAU"
        )
      ).toEqual(true);
    });
  });

  describe("Should process valid campaigns", () => {
    const mockedMessage = {
      say: (message) => message,
    };
    const campaign: Campaign = {
      isFinished: false,
      createdAt: new Date().toISOString(),
      description: "ddd",
      roll20Link: "https://app.roll20.net/join/2883710/mEiLZw",
      name: "lll",
      playlist: `https://www.youtube.com/playlist?list=DSDSDSDSDSDSDSDSDS`,
      image:
        "https://www.pinpng.com/pngs/m/676-6768344_ohayo-face-xayah-hd-png-download-png-download.png",
    };
    beforeAll(() => {
      container.rebind<DataService>(TYPES.DataService).to(DatabaseApiClient);
    });

    it(
      "Should create a campaign with all parameters",
      async () => {
        const res = await command.run(mockedMessage, {
          name: campaign.name,
          description: campaign.description,
          link: campaign.roll20Link,
          image: campaign.image,
        });
        expect(res).not.toBeUndefined();
      },
      5 * 60 * 1000
    );
    it("Should create a campaign with no image specified", async () => {
      const res = await command.run(mockedMessage, {
        name: campaign.name,
        description: campaign.description,
        link: campaign.roll20Link,
      });
      expect(res).not.toBeUndefined();
    });
    describe("A Youtube Quota exception is thrown", () => {
      beforeAll(() => {
        jest
          .spyOn(MockYoutubeService.prototype, "createPlaylist")
          .mockImplementation(() => {
            throw new YoutubeQuotaError();
          });
      });
      it("Should gracefully exit", async () => {
        const res = await command.run(mockedMessage, {
          name: campaign.name,
          description: campaign.description,
          link: campaign.roll20Link,
        });
        expect(res).toBeUndefined();
      });
    });
    describe("An Unknown Youtube exception is thrown", () => {
      beforeAll(() => {
        jest
          .spyOn(MockYoutubeService.prototype, "createPlaylist")
          .mockImplementation(() => {
            throw new Error();
          });
      });
      it("Should gracefully exit", async () => {
        const res = await command.run(mockedMessage, {
          name: campaign.name,
          description: campaign.description,
          link: campaign.roll20Link,
        });
        expect(res).toBeUndefined();
      });
    });
    describe("An Unknown Database exception is thrown", () => {
      beforeAll(() => {
        jest
          .spyOn(DatabaseApiClient.prototype, "createCampaign")
          .mockImplementation(() => {
            throw new Error();
          });
      });
      it("Should gracefully exit", async () => {
        const res = await command.run(mockedMessage, {
          name: campaign.name,
          description: campaign.description,
          link: campaign.roll20Link,
        });
        expect(res).toBeUndefined();
      });
    });
  });
});
