import "reflect-metadata";
import { container } from "../inversify.config";
import { MainRecorderAPI } from "../@types/recording-service";
import { TYPES } from "../types";
import { hrtime } from "process";
import { RecorderService } from "./recorder";

describe("Recorder", () => {
  let recorder: MainRecorderAPI;
  beforeAll(() => {
    container
      .rebind<MainRecorderAPI>(TYPES.RecordingService)
      .toConstantValue(new RecorderService(null, null));
    recorder = container.get<MainRecorderAPI>(TYPES.RecordingService);
  });
  it(
    "Should download audio",
    async () => {
      await recorder.processRecording();
    },
    15 * 60 * 1000
  );
  describe("Should calculate the offset between two times", () => {
    it("When audio has started before video", () => {
      const audio = hrtime();
      const video = hrtime();
      video[0] += 1;
      const res = recorder.calculateOffset(video, audio);
      console.log(res);
      expect(res).toBeGreaterThanOrEqual(1);
    });
    it("When video has started before audio", () => {
      const audio = hrtime();
      const video = hrtime();
      audio[0] += 1;
      const res = recorder.calculateOffset(video, audio);
      console.log(res);
      expect(res).toBeLessThanOrEqual(-1);
    });
  });
});
