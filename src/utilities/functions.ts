// A function that emits a side effect and does not return anything.
export type Procedure = (...args: any[]) => void;

export type Options = {
  isImmediate: boolean;
};

/**
 * Wait until "waitMilliseconds" ms since the last call to call "func"
 * @param func
 * @param waitMilliseconds
 * @param options
 */
export function debounce<F extends Procedure>(
  func: F,
  waitMilliseconds = 50,
  options: Options = {
    isImmediate: false,
  }
): (this: ThisParameterType<F>, ...args: Parameters<F>) => void {
  let timeoutId: ReturnType<typeof setTimeout> | undefined;

  return function (this: ThisParameterType<F>, ...args: Parameters<F>) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const context = this;

    const doLater = function () {
      timeoutId = undefined;
      if (!options.isImmediate) {
        func.apply(context, args);
      }
    };

    const shouldCallNow = options.isImmediate && timeoutId === undefined;

    if (timeoutId !== undefined) {
      clearTimeout(timeoutId);
    }

    timeoutId = setTimeout(doLater, waitMilliseconds);

    if (shouldCallNow) {
      func.apply(context, args);
    }
  };
}

/**
 * Limit the number of time "func" can be called in a given "timeWindow" of time (ms)
 * @param func
 * @param timeWindow
 */
export function throttle(
  // eslint-disable-next-line @typescript-eslint/ban-types
  func: Function,
  timeWindow: number
): (...args: any[]) => void {
  let inThrottle: boolean;

  return function (this: any): any {
    // eslint-disable-next-line prefer-rest-params
    const args = arguments;
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const context = this;

    if (!inThrottle) {
      inThrottle = true;
      func.apply(context, args);
      setTimeout(() => (inThrottle = false), timeWindow);
    }
  };
}
