import { RedisAPI } from "../../@types/redis-service";
import { injectable } from "inversify";
import { RedisMessage } from "../../@types/redis-message";
import { EventEmitter } from "events";
import { RedisOptions } from "ioredis";

@injectable()
export class MockRedisService extends EventEmitter implements RedisAPI {
  constructor(options: RedisOptions) {
    super();
  }
  publish(channel: string, payload: Record<string, any>) {
    console.log(`REDIS : Published ${payload} on channel ${channel}`);
  }

  subscribe(channel: string) {
    return;
  }

  unsubscribe(channel: string): Promise<void> {
    return Promise.resolve(undefined);
  }
  requestPlayerUpdate(message: RedisMessage, timeout?: number) {
    return;
  }

  waitFor(channel: string): Promise<RedisMessage> {
    return Promise.resolve(undefined);
  }
}
