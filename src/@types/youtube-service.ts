// eslint-disable-next-line @typescript-eslint/camelcase
import { youtube_v3 } from "googleapis";
// eslint-disable-next-line @typescript-eslint/camelcase
import Schema$Playlist = youtube_v3.Schema$Playlist;
// eslint-disable-next-line @typescript-eslint/camelcase
import Schema$PlaylistItem = youtube_v3.Schema$PlaylistItem;
// eslint-disable-next-line @typescript-eslint/camelcase
import Schema$LiveBroadcast = youtube_v3.Schema$LiveBroadcast;
// eslint-disable-next-line @typescript-eslint/camelcase
import Schema$Video = youtube_v3.Schema$Video;
// eslint-disable-next-line @typescript-eslint/camelcase
import Schema$ThumbnailSetResponse = youtube_v3.Schema$ThumbnailSetResponse;
import EventEmitter = NodeJS.EventEmitter;

export interface YoutubeItemOptions {
  title: string;
  description: string;
  privacyStatus?: string;
  categoryId?: string;
}

export interface YoutubeInitOptions {
  clientId: string;
  clientSecret: string;
  redirectUris: string[];
  accessToken: {
    access_token: string;
    token_type: string;
    refresh_token: string;
    expiry_date: number;
  };
}

export interface UploadProgress {
  max: number;
  current: number;
}

export interface YoutubeAPI extends EventEmitter {
  createPlaylist(option: YoutubeItemOptions): Promise<Schema$Playlist>;

  addVideoToPlaylist(
    videoId: string,
    playlistId: string
  ): Promise<Schema$PlaylistItem>;

  deletePlaylist(playlist: string): Promise<void>;

  getCurrentBroadcast(): Promise<Schema$LiveBroadcast>;

  getIdFromUrl(id: string): string;

  getVideoDuration(id: string): Promise<number>;

  updateVideo(id: string, parameters: YoutubeItemOptions): Promise<void>;

  uploadVideo(path: string, options: YoutubeItemOptions): Promise<Schema$Video>;

  updateThumbnail(
    id: string,
    path: string
  ): Promise<Schema$ThumbnailSetResponse>;

  deleteVideo(videoId: string): Promise<void>;
}
