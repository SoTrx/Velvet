import { EventEmitter } from "events";
import { google, youtube_v3 } from "googleapis";
import { parse, toSeconds } from "../utilities/duration-converter";
import { GaxiosResponse } from "gaxios";
import {
  UploadProgress,
  YoutubeAPI,
  YoutubeInitOptions,
  YoutubeItemOptions,
} from "../@types/youtube-service";
import { injectable } from "inversify";
import { createReadStream, statSync } from "fs";

import Schema$Playlist = youtube_v3.Schema$Playlist;
import Schema$PlaylistItem = youtube_v3.Schema$PlaylistItem;
import Schema$LiveBroadcastListResponse = youtube_v3.Schema$LiveBroadcastListResponse;
import Schema$LiveBroadcast = youtube_v3.Schema$LiveBroadcast;
import Schema$Video = youtube_v3.Schema$Video;
import Schema$ThumbnailSetResponse = youtube_v3.Schema$ThumbnailSetResponse;
import { debounce, throttle } from "../utilities/functions";

export class YoutubeQuotaError extends Error {}

@injectable()
export class YoutubeService extends EventEmitter implements YoutubeAPI {
  private readonly oauth2Client;
  private currentToken: string;
  private scopes: Array<string>;

  constructor(options: YoutubeInitOptions) {
    super();

    this.oauth2Client = new google.auth.OAuth2(
      options.clientId,
      options.clientSecret,
      options.redirectUris[0]
    );
    this.oauth2Client.credentials = options.accessToken;
    google.options({ auth: this.oauth2Client });

    this.currentToken = this.oauth2Client.credentials.access_token;

    this.oauth2Client.on("tokens", (tokens) => {
      this.currentToken = tokens.access_token;
    });

    this.scopes = [
      "https://www.googleapis.com/auth/youtube.upload",
      "https://www.googleapis.com/auth/youtube",
    ];
  }

  /**
   * Get the ID of a ressource given it's URL
   * @param url Url of the ressource to extract the ID from
   * @return Ressource ID or undefined
   */
  getIdFromUrl(url: string): string {
    const playlistReg = /^.*(?:youtube\.com|youtu\.be)\/playlist\?list=(.*)$/;
    const videoReg = /^.*(?:youtube\.com|youtu\.be)\/watch\?v=(.*)$/;

    if (playlistReg.test(url)) {
      return url.match(playlistReg)[1];
    } else if (videoReg.test(url)) {
      return url.match(videoReg)[1];
    }
    return undefined;
  }

  /**
   * Create a new playlist with the given option on the Youtube Channel
   * @param options
   * @return Created playlist data
   * @throws If title is not specified or Youtube API Quota is exceeded
   */
  async createPlaylist(options: YoutubeItemOptions): Promise<Schema$Playlist> {
    if (!options.title) {
      throw new Error("A title is required");
    }
    const youtube = this._initialize();

    const result: GaxiosResponse<Schema$Playlist> = (await youtube.playlists
      .insert({
        part: ["snippet, status"],
        requestBody: {
          snippet: {
            title: options.title,
            description:
              options.description || "Une très bonne chronique comme TPMP",
          },
          status: {
            privacyStatus: "unlisted",
          },
        },
      })
      .catch((err) => {
        if (err.code == 403) {
          throw new YoutubeQuotaError("Quota Youtube");
        } else {
          throw err;
        }
      })) as GaxiosResponse<Schema$Playlist>;
    return result.data;
  }

  /**
   * Add a video (already uploaded) to a youtube playlist
   * @param videoId
   * @param playlistId
   * @throws Error if either parameters are not specified
   * @returns Updated playlist data
   */
  async addVideoToPlaylist(
    videoId: string,
    playlistId: string
  ): Promise<Schema$PlaylistItem> {
    if (!videoId || !playlistId) {
      throw new Error("VideoRecorder id and playlist id are required !");
    }
    const youtube = this._initialize();

    const res: GaxiosResponse<Schema$PlaylistItem> = (await youtube.playlistItems
      .insert({
        part: ["snippet"],
        requestBody: {
          snippet: {
            resourceId: {
              kind: "youtube#video",
              videoId: videoId,
            },
            playlistId: playlistId,
          },
        },
      })
      .catch((err) => {
        if (err.code == 403) {
          throw new YoutubeQuotaError("Quota Youtube");
        }
      })) as GaxiosResponse<Schema$PlaylistItem>;
    return res.data;
  }

  /**
   * Delete the given playlist on the Youtube Channel
   * @param playlistId
   */
  async deletePlaylist(playlistId: string): Promise<void> {
    const youtube = this._initialize();

    await youtube.playlists
      .delete({
        id: playlistId,
      })
      .catch(console.error);
  }

  /**
   * Return the URL of the broadcast currently streamed to youtube
   * @returns info about the live broadcast
   */
  async getCurrentBroadcast(): Promise<Schema$LiveBroadcast> {
    const youtube = this._initialize();
    const result: GaxiosResponse<Schema$LiveBroadcastListResponse> = await youtube.liveBroadcasts.list(
      {
        part: ["snippet"],
        broadcastStatus: "active",
        broadcastType: "all",
      }
    );
    if (result.data && result.data.items) {
      return result.data.items[0];
    }
    return null;
  }

  /**
   * Return the duration of a Youtube video
   * @param id Youtube id of the video
   * @returns Duration of the video, 0 in the case we don't know
   */
  async getVideoDuration(id: string): Promise<number> {
    const youtube = this._initialize();
    const result = await youtube.videos.list({
      part: ["contentDetails"],
      id: [id],
    });
    if (result.data && result.data.items && result.data.items[0]) {
      const durationObject = parse(
        result.data.items[0].contentDetails.duration
      );
      return toSeconds(durationObject);
    }
    return 0;
  }

  async uploadVideo(
    path: string,
    options: YoutubeItemOptions
  ): Promise<Schema$Video> {
    const youtube = this._initialize();
    const fileSize = statSync(path).size;
    const throttledProgress = throttle(
      (progress) => this.emit("uploadProgress", progress),
      3000
    );

    const res = await youtube.videos.insert(
      {
        part: ["id,snippet,status"],
        notifySubscribers: false,
        //autoLevels : true,
        requestBody: {
          snippet: {
            title:
              options.title ||
              "Dans un monde alternatif où nous sommes tous des marmottes",
            description:
              options.description ||
              "Personne ne lit les descriptions de toute façon",
          },
          status: {
            privacyStatus: options.privacyStatus || "unlisted",
          },
        },
        media: {
          body: createReadStream(path),
        },
      },
      {
        onUploadProgress: (evt) => {
          console.log(evt.bytesRead);
          const progress: UploadProgress = {
            current: evt.bytesRead,
            max: fileSize,
          };
          // Only emit progress once every 3 seconds
          throttledProgress(progress);
        },
      }
    );
    return res.data;
  }

  /**
   * Update a video parameters
   * @param id Youtube video Id
   * @param parameters Things to change
   */
  async updateVideo(id: string, parameters: YoutubeItemOptions): Promise<void> {
    const youtube = this._initialize();
    await youtube.videos.update({
      part: ["snippet"],
      requestBody: {
        id: id,
        snippet: {
          title: parameters.title || "Grosse vidéo",
          categoryId: parameters.categoryId || "24",
        },
      },
    });
  }

  async updateThumbnail(
    id: string,
    path: string
  ): Promise<Schema$ThumbnailSetResponse> {
    const youtube = this._initialize();
    const res = await youtube.thumbnails.set({
      videoId: id,
      media: {
        body: createReadStream(path),
      },
    });
    return res.data;
  }

  /**
   * Delete a VideoRecorder from the Youtube Channel
   * @param videoId
   */
  async deleteVideo(videoId: string): Promise<void> {
    const youtube = this._initialize();

    await youtube.videos.delete({
      id: videoId,
    });
  }

  /**
   * Initialize Youtube functions from the Google API
   * @returns Initialized youtube Object
   */
  private _initialize(): youtube_v3.Youtube {
    return google.youtube({
      version: "v3",
      maxRedirects: 10,
      auth: this.oauth2Client,
    });
  }
}
