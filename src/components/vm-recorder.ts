import {
  AccurateTime,
  Roll20RecorderAPI,
  Roll20RecorderOptions,
} from "../@types/recording-service";
import { RedisAPI } from "../@types/redis-service";
import { inject, injectable } from "inversify";
import { Campaign } from "../@types/api";
import { RedisMessage } from "../@types/redis-message";
import { RecordingMode } from "../services/recorder";
import { TYPES } from "../types";

export enum PubChannels {
  StartStreaming = "START_RECORDING_VM",
  StopStreaming = "STOP_RECORDING_VM",
}

export enum SubChannels {
  StreamingBegan = "STARTED_RECORDING_VM",
  StreamingStopped = "STOPPED_RECORDING_VM",
}
export class VmRecorderError extends Error {}

@injectable()
export class VmRecorder implements Roll20RecorderAPI {
  public readonly recordingPath;
  recordingMode = RecordingMode.VideoAndAudio;
  private campaign: Campaign;

  constructor(
    @inject(TYPES.RedisService) private redis: RedisAPI,
    options: Roll20RecorderOptions
  ) {
    this.recordingPath = options.streamingServerUrl;
  }
  async startRecording(campaign: Campaign): Promise<void> {
    this.campaign = campaign;
    const message = {
      hasError: false,
      campaignId: String(this.campaign.id),
      data: { gameUrl: this.campaign.roll20Link },
    };
    this.redis.publish(PubChannels.StartStreaming, message);
    await this.waitForMessage(SubChannels.StreamingBegan);
  }
  private async waitForMessage(
    channel: SubChannels | PubChannels
  ): Promise<RedisMessage> {
    try {
      return await this.redis.waitFor(channel);
    } catch (e) {
      throw new VmRecorderError(e);
    }
  }

  async stopRecording(): Promise<AccurateTime> {
    const message = {
      hasError: false,
      campaignId: String(this.campaign.id),
      data: { gameUrl: this.campaign.roll20Link },
    };
    const answer = this.waitForMessage(SubChannels.StreamingStopped);
    // Actually wait because the answer is too fast
    await new Promise<void>((res) => setTimeout(() => res(), 1500));
    this.redis.publish(PubChannels.StopStreaming, message);
    const res = await answer;
    console.log("VM");
    console.log(res);
    // The actual time is considered negligeable
    return undefined;
  }
}
