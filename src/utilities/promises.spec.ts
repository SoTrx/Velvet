import { allWithProgress } from "./promises";

describe("Promises utilities", () => {
  describe("All with progress ", () => {
    function test(ms) {
      return new Promise((resolve) => {
        setTimeout(() => {
          console.log(`Waited ${ms}`);
          resolve();
        }, ms);
      });
    }
    it("Should show the progress of the all() method", async () => {
      await allWithProgress(
        [test(1000), test(9000), test(4000), test(5500)],
        (p) => {
          console.log(`% Done = ${p.toFixed(2)}`);
        }
      );
    }, 10000);
  });
});
