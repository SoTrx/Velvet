import { ThumbnailGeneratorAPI } from "../@types/thumbnail-generator-API";
import axios from "axios";
import { injectable } from "inversify";
import { tmpdir } from "os";
import { createWriteStream, promises } from "fs";
import { hrtime } from "process";

@injectable()
export class ThumbnailGenerator implements ThumbnailGeneratorAPI {
  private static readonly GENERATOR_URL = "https://thumbnail.pocot.fr";
  private static readonly ENDPOINTS = {
    view: (sessionId: number) =>
      `${ThumbnailGenerator.GENERATOR_URL}/${sessionId}`,
    sample: (campaignId: number, sessionTitle) =>
      `${ThumbnailGenerator.GENERATOR_URL}/sample/${campaignId}/${sessionTitle}`,
    download: (sessionId: number) =>
      `${ThumbnailGenerator.GENERATOR_URL}/download/${sessionId}`,
  };

  private generateRandomPath() {
    return `${tmpdir()}/thumbnail-${hrtime().join("_")}.png`;
  }
  async generateThumbnailForSession(id: number): Promise<string> {
    const data = (
      await axios.get(ThumbnailGenerator.ENDPOINTS.download(id), {
        responseType: "stream",
      })
    ).data;
    const path = this.generateRandomPath();
    const writer = createWriteStream(path);
    await data.pipe(writer);
    return new Promise((resolve, reject) => {
      writer.on("finish", () => resolve(path));
      writer.on("error", reject);
    });
  }

  async generateSampleThumbnailForCampaign(
    campaignId: number,
    sessionTitle: string
  ): Promise<string> {
    const data = (
      await axios.get(
        ThumbnailGenerator.ENDPOINTS.sample(campaignId, sessionTitle),
        {
          responseType: "stream",
        }
      )
    ).data;
    const path = this.generateRandomPath();
    const writer = createWriteStream(path);
    await data.pipe(writer);
    return new Promise((resolve, reject) => {
      writer.on("finish", () => resolve(path));
      writer.on("error", reject);
    });
  }
}
