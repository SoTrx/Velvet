import { progressBar } from "./user-interaction";

describe("User Interaction", () => {
  it("Should display the progress bar", () => {
    const pg = progressBar(60, 100, 50);
    console.log(pg);
  });

  it("Should handle limit cases", () => {
    const pg = progressBar(100, 100, 50);
    console.log(pg);
  });
});
