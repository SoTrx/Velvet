export const TYPES = {
  ScrapperService: Symbol.for("ScrapperService"),
  DataService: Symbol.for("DataService"),
  RecordingService: Symbol.for("RecordingService"),
  AudioRecorder: Symbol.for("AudioRecorder"),
  VideoRecorder: Symbol.for("VideoRecorder"),
  VideoRecorderFactory: Symbol.for("Factory<VideoRecorder>"),
  YoutubeService: Symbol.for("YoutubeService"),
  RedisService: Symbol.for("RedisService"),
  ThumbnailService: Symbol.for("ThumbnailService"),
  Velvet: Symbol.for("Velvet"),
};
