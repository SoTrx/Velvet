import "reflect-metadata";
import { YoutubeService } from "./youtube-service";
import { container } from "../inversify.config";
import { TYPES } from "../types";
import { resolve } from "path";

describe("Youtube Service", () => {
  const yt = container.get<YoutubeService>(TYPES.YoutubeService);
  it("Should be able to upload small videos", async () => {
    const video = await yt.uploadVideo("/home/lucas/Documents/Velvet/meh.flv", {
      description: "OLALA2",
      title: "Miaou/20",
      privacyStatus: "unlisted",
    });
  }, 180000);

  it("Should get the length of a video", async () => {
    const duration = await yt.getVideoDuration("9MGGAZyq1Mw");
    expect(duration).toBe(4 * 60 + 52);
  }, 180000);
  it("Should update a video thumbnail", async () => {
    const thumbnail = await yt.updateThumbnail(
      "ydQEiRrE1ZA",
      resolve(__dirname, "../assets/images/session_287_sample_thumbnail.png")
    );
    console.log(thumbnail);
  }, 180000);
});
