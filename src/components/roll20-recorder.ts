import {
  AccurateTime,
  Roll20RecorderAPI,
  Roll20RecorderOptions,
} from "../@types/recording-service";
import { RedisAPI } from "../@types/redis-service";
import { inject, injectable } from "inversify";
import { Campaign } from "../@types/api";
import { RedisMessage } from "../@types/redis-message";
import { RecordingMode } from "../services/recorder";
import { TYPES } from "../types";

export enum PubChannels {
  StartStreaming = "startStreamingRoll20Game",
  StopStreaming = "stopStreamingRoll20Game",
}

export enum SubChannels {
  StreamingBegan = "streamingRoll20GameBegan",
  StreamingStopped = "streamingRoll20GameStopped",
}
export class Roll20RecorderError extends Error {}

@injectable()
export class Roll20Recorder implements Roll20RecorderAPI {
  public readonly recordingPath;
  recordingMode = RecordingMode.VideoAndAudio;
  private campaign: Campaign;

  constructor(
    @inject(TYPES.RedisService) private redis: RedisAPI,
    options: Roll20RecorderOptions
  ) {
    this.recordingPath = options.streamingServerUrl;
  }
  async startRecording(campaign: Campaign): Promise<void> {
    this.campaign = campaign;
    const message: RedisMessage = {
      hasError: false,
      campaignId: String(this.campaign.id),
      campaignRoll20Ids: [this.campaign.roll20Link.match(/\/(\d+)/)[1]],
      data: { gameUrl: this.campaign.roll20Link },
    };
    this.redis.publish(PubChannels.StartStreaming, message);
    await this.waitForMessage(SubChannels.StreamingBegan);
  }
  private async waitForMessage(
    channel: SubChannels | PubChannels
  ): Promise<RedisMessage> {
    try {
      return await this.redis.waitFor(channel);
    } catch (e) {
      throw new Roll20RecorderError(e);
    }
  }

  async stopRecording(): Promise<AccurateTime> {
    const message: RedisMessage = {
      hasError: false,
      campaignId: String(this.campaign.id),
      campaignRoll20Ids: [this.campaign.roll20Link.match(/\/(\d+)/)[1]],
      data: { gameUrl: this.campaign.roll20Link },
    };
    const answer = this.waitForMessage(SubChannels.StreamingStopped);
    // Actually wait because the answer is too fast
    await new Promise<void>((res) => setTimeout(() => res(), 1500));
    this.redis.publish(PubChannels.StopStreaming, message);
    const res = await answer;
    console.log("ROll20");
    console.log(res);
    return res.data.startDate;
  }
}
