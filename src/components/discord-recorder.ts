import {
  AccurateTime,
  DiscordRecorderAPI,
  DiscordRecorderOptions,
} from "../@types/recording-service";
import { inject, injectable } from "inversify";
import { RedisAPI } from "../@types/redis-service";
import { RedisMessage } from "../@types/redis-message";
import { RecordingMode } from "../services/recorder";
import axios from "axios";
import { httpAdapter } from "axios/lib/adapters/http";
import { Readable } from "stream";
import { TYPES } from "../types";

export enum PubChannels {
  StartRecording = "startRecordingDiscord",
  StopRecording = "stopRecordingDiscord",
}

export enum SubChannels {
  RecordingBegan = "recordingDiscordBegan",
  RecordingStopped = "recordingDiscordStopped",
  RecordingErrored = "recordingDiscordErrored",
}

export class DiscordRecorderError extends Error {}

@injectable()
export class DiscordRecorder implements DiscordRecorderAPI {
  recordingMode: RecordingMode.AudioOnly;
  public recordId;
  // Record that have failed to complete
  private oldRecordsIds = [];
  private readonly cookingBaseUrl;
  //private static readonly cookingBaseUrl = "pandora-cooking-server:3004";
  private readonly endpoints = {
    flacMixed: (id): string => `${this.cookingBaseUrl}/${id}?format=flac`,
  };

  private voiceChannelId: string;

  constructor(
    @inject(TYPES.RedisService) private redis: RedisAPI,
    options: DiscordRecorderOptions
  ) {
    this.cookingBaseUrl = options.cookingServerUrl;
  }

  async startRecording(voiceChannelId: string): Promise<void> {
    this.voiceChannelId = voiceChannelId;
    const message = {
      hasError: false,
      data: { voiceChannelId: this.voiceChannelId },
    };
    this.redis.publish(PubChannels.StartRecording, message);
    const returnPayload = await this.waitForMessage(SubChannels.RecordingBegan);
    this.redis.subscribe(SubChannels.RecordingErrored);
    this.redis.on("message", ([channel, message]) => {
      if (channel === SubChannels.RecordingErrored)
        this.handleRecordingError(message);
    });
    this.recordId = ((returnPayload as unknown) as RedisMessage).data.recordId;
  }

  async stopRecording(): Promise<AccurateTime> {
    const message = {
      hasError: false,
      data: null,
    };
    this.redis.publish(PubChannels.StopRecording, message);
    const res = await this.waitForMessage(SubChannels.RecordingStopped);
    await this.redis.unsubscribe(SubChannels.RecordingErrored);
    console.log("Pandora");
    console.log(res);
    return res.data.startDate;
  }

  private async handleRecordingError(message: Record<string, any>) {
    console.log(
      `Audio recorder errored with error ${message}, attempting disaster recovery`
    );
    this.oldRecordsIds.push(this.recordId);
    // Wait for the recorder to go back to a normal state
    await new Promise((res) => setTimeout(res, 5000));
    await this.startRecording(this.voiceChannelId);
  }

  private async waitForMessage(
    channel: SubChannels | PubChannels
  ): Promise<RedisMessage> {
    try {
      return await this.redis.waitFor(channel);
    } catch (e) {
      throw new DiscordRecorderError(e);
    }
  }

  /**
   * Get mixed audio stream for the whole recording
   */
  async getAudioStream(recordId = this.recordId): Promise<Readable> {
    return (
      await axios.get(this.endpoints.flacMixed(recordId), {
        responseType: "stream",
        adapter: httpAdapter,
      })
    ).data;
  }

  async getAudioStreams(): Promise<Readable[]> {
    const allRecords = [];
    allRecords.push(...this.oldRecordsIds);
    allRecords.push(this.recordId);
    return Promise.all(
      allRecords.map(async (r) => await this.getAudioStream(r))
    );
  }
}
