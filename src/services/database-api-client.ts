import { injectable } from "inversify";
import { DataService, DataServiceOptions } from "../@types/data-service";
import {
  Campaign,
  Font,
  GMCampaign,
  Player,
  PlayerCampaign,
  Session,
} from "../@types/api";
import axios from "axios";

@injectable()
export class DatabaseApiClient implements DataService {
  private readonly apiBaseUrl;
  private readonly endpoints = {
    players: () => `${this.apiBaseUrl}/players`,
    characters: () => `${this.apiBaseUrl}/characters`,
    campaigns: () => `${this.apiBaseUrl}/campaigns`,
    playerCampaigns: () => `${this.apiBaseUrl}/player-campaigns`,
    gmCampaigns: () => `${this.apiBaseUrl}/gm-campaigns`,
    rolls: () => `${this.apiBaseUrl}/rolls`,
    sessions: () => `${this.apiBaseUrl}/sessions`,
    addSession: () => `${this.apiBaseUrl}/sessions`,
    characterRecords: () => `${this.apiBaseUrl}/character-records`,
    createCampaign: () => `${this.apiBaseUrl}/campaigns`,
    updatePlayers: () => `${this.apiBaseUrl}/player-campaigns/executeUpdate`,
    fonts: () => `${this.apiBaseUrl}/fonts`,
  };
  constructor(options: DataServiceOptions) {
    this.apiBaseUrl = options.dataBaseUrl;
  }
  async getCampaigns(): Promise<Campaign[]> {
    return (await axios.get<Campaign[]>(this.endpoints.campaigns())).data;
  }

  async getGmCampaigns(): Promise<GMCampaign[]> {
    return (await axios.get<GMCampaign[]>(this.endpoints.gmCampaigns())).data;
  }

  async getPlayers(): Promise<Player[]> {
    return (await axios.get<Player[]>(this.endpoints.players())).data;
  }

  async getPlayersCampaign(): Promise<PlayerCampaign[]> {
    return (await axios.get<PlayerCampaign[]>(this.endpoints.playerCampaigns()))
      .data;
  }

  async getPlayersOfCampaign(campaignId: string): Promise<Player[]> {
    const playerIds = (await this.getPlayersCampaign())
      .filter((pC) => campaignId == pC.CampaignId)
      .map((pC) => pC.PlayerIdRoll20.toString());
    return (await this.getPlayers()).filter((p) =>
      playerIds.includes(String(p.idRoll20))
    );
  }

  async getSessions(): Promise<Session[]> {
    return (await axios.get<Session[]>(this.endpoints.sessions())).data;
  }
  async getFonts(): Promise<Font[]> {
    const fonts = (await axios.get<Font[]>(this.endpoints.fonts())).data;
    return fonts.map(this.convertFont);
  }
  private convertFont(font: Font): Font {
    font.fontfile = Buffer.from((font.fontfile as unknown) as string, "base64");
    return font;
  }
  async getFont(id: number): Promise<Font> {
    const font = (await axios.get<Font>(`${this.endpoints.fonts}/${id}`)).data;
    return this.convertFont(font);
  }

  async updateFont(campaignId: number, content: Buffer): Promise<void> {
    const fontObject = {
      campaignid: campaignId,
      fontfile: content.toString("base64"),
    };
    return axios.put(`${this.endpoints.fonts}/${campaignId}`, fontObject);
  }
  async createFont(campaignId: number, content: Buffer): Promise<Font> {
    const fontObject = {
      campaignid: campaignId,
      fontfile: content.toString("base64"),
    };
    return (await axios.post<Font>(`${this.endpoints.fonts}`, fontObject)).data;
  }

  async getCampaignsForUser(userId: string): Promise<Campaign[]> {
    const playerIds = (await this.getPlayers())
      .filter((p) => p.idDiscord !== null && p.idDiscord?.toString() === userId)
      .map((p) => p.idRoll20.toString());

    const gmCampaingsIds = (await this.getGmCampaigns())
      .filter((gmC) => playerIds.includes(gmC.roll20Id))
      .map((gmC) => gmC.campaignId.toString());
    return (await this.getCampaigns()).filter((c) =>
      gmCampaingsIds.includes(c.id.toString())
    );
  }

  async getGmsOfCampaign(campaignId: string): Promise<Player[]> {
    const gmIds = (await this.getGmCampaigns())
      .filter((gmC) => campaignId == gmC.campaignId)
      .map((gmC) => gmC.roll20Id.toString());
    return (await this.getPlayers()).filter((p) =>
      gmIds.includes(String(p.idRoll20))
    );
  }

  async addSession(session: Session): Promise<Session> {
    session.campaignid = Number(session.campaignid);
    session.index = Number(session.index);
    session.duration = session.duration.toString();
    return (await axios.post<Session>(this.endpoints.addSession(), session))
      .data;
  }

  async getSessionsForCampaign(campaignId: string): Promise<Session[]> {
    return (await this.getSessions())?.filter(
      (s) => s.campaignid.toString() === campaignId
    );
  }

  async createCampaign(campaign: Campaign): Promise<Campaign> {
    return (
      await axios.post<Campaign>(this.endpoints.createCampaign(), campaign)
    ).data;
  }

  async updatePlayersOfCampaign(
    campaignId: string,
    players: Partial<any>[]
  ): Promise<void> {
    await axios.post(this.endpoints.updatePlayers(), {
      campaignId: campaignId,
      hasError: false,
      data: players,
    });
  }

  async patchCampaignById(
    campaignId: string,
    patch: Partial<Campaign>
  ): Promise<void> {
    await axios.patch(`${this.endpoints.campaigns()}/${campaignId}`, patch);
  }
}
