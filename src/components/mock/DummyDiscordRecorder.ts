import {
  AccurateTime,
  DiscordRecorderAPI,
} from "../../@types/recording-service";
import { Readable } from "stream";
import axios from "axios";
import { httpAdapter } from "axios/lib/adapters/http";
import { RecordingMode } from "../../services/recorder";

export class DummyDiscordRecorder implements DiscordRecorderAPI {
  recordId = "872660673";
  recordingMode: RecordingMode.AudioOnly;
  recordingPath: string;
  private static readonly COOKING_BASE_URL = "https://cooking.pandora.pocot.fr";
  //private static readonly COOKING_BASE_URL = "pandora:3004";
  private static readonly ENDPOINTS = {
    flacMixed: (id): string =>
      `${DummyDiscordRecorder.COOKING_BASE_URL}/${id}?format=flac`,
  };
  async getAudioStream(): Promise<Readable> {
    return (
      await axios.get(DummyDiscordRecorder.ENDPOINTS.flacMixed(this.recordId), {
        responseType: "stream",
        adapter: httpAdapter,
      })
    ).data;
  }

  startRecording(voiceChannelId: string): void {
    return;
  }

  getDevices(): string[] {
    return [];
  }

  getAudioStreams(): Promise<Readable[]> {
    return Promise.resolve([]);
  }

  stopRecording(): Promise<AccurateTime> {
    return Promise.resolve(undefined);
  }
}
