export interface ThumbnailGeneratorAPI {
  generateThumbnailForSession(id: number): Promise<string>;
  generateSampleThumbnailForCampaign(
    campaignId: number,
    sessionTitle: string
  ): Promise<string>;
}
