import Global = NodeJS.Global;
import { MainRecorderAPI } from "./recording-service";
import { Campaign } from "./api";

export interface GlobalExt extends Global {
  recorderInstance: MainRecorderAPI;
  recordedCampaign: Campaign;
}
