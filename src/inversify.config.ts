import { Container, interfaces } from "inversify";
import { DatabaseApiClient } from "./services/database-api-client";
import { Velvet } from "./Velvet";
import { TYPES } from "./types";
import { DataService } from "./@types/data-service";
import { RecorderService } from "./services/recorder";
import {
  DiscordRecorderAPI,
  MainRecorderAPI,
  Roll20RecorderAPI,
} from "./@types/recording-service";
import { YoutubeAPI } from "./@types/youtube-service";
import { YoutubeService } from "./services/youtube-service";
import { RedisAPI } from "./@types/redis-service";
import { RedisService } from "./services/redis-service";
import { ThumbnailGeneratorAPI } from "./@types/thumbnail-generator-API";
import { ThumbnailGenerator } from "./services/thumbnail-generator";
import { DiscordRecorder } from "./components/discord-recorder";
import { Roll20Recorder } from "./components/roll20-recorder";
import { env } from "process";
import { ScrapperServiceAPI } from "./@types/scrapper-service";
import { ScrapperApiClient } from "./services/scrapper-api-client";
import { VmRecorder } from "./components/vm-recorder";

export const container = new Container();

container
  .bind<ScrapperServiceAPI>(TYPES.ScrapperService)
  .toConstantValue(new ScrapperApiClient(env.SCRAPPER_SERVER_URL));

container
  .bind<DataService>(TYPES.DataService)
  .toConstantValue(new DatabaseApiClient({ dataBaseUrl: env.DATA_SERVER_URL }));

container.bind<RedisAPI>(TYPES.RedisService).toDynamicValue(
  () =>
    new RedisService({
      host: env.REDIS_HOST,
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      lazyConnect: env.REDIS_LAZY_CONNECT,
    })
);

container
  .bind<ThumbnailGeneratorAPI>(TYPES.ThumbnailService)
  .to(ThumbnailGenerator);

container.bind<DiscordRecorderAPI>(TYPES.AudioRecorder).toConstantValue(
  new DiscordRecorder(container.get<RedisAPI>(TYPES.RedisService), {
    cookingServerUrl: env.COOKING_SERVER_URL,
  })
);

container
  .bind<Roll20RecorderAPI>(TYPES.VideoRecorder)
  .toConstantValue(
    new Roll20Recorder(container.get<RedisAPI>(TYPES.RedisService), {
      streamingServerUrl: env.STREAMING_SERVER_URL,
    })
  )
  .whenTargetNamed("roll20");

container
  .bind<Roll20RecorderAPI>(TYPES.VideoRecorder)
  .toConstantValue(
    new VmRecorder(container.get<RedisAPI>(TYPES.RedisService), {
      streamingServerUrl: env.STREAMING_SERVER_URL,
    })
  )
  .whenTargetNamed("vm");

container
  .bind<interfaces.Factory<Roll20RecorderAPI>>(TYPES.VideoRecorderFactory)
  .toFactory<Roll20RecorderAPI>((context) => {
    return (named: string) => {
      return context.container.getNamed<Roll20RecorderAPI>(
        TYPES.VideoRecorder,
        named
      );
    };
  });
container.bind<MainRecorderAPI>(TYPES.RecordingService).to(RecorderService);

container.bind<YoutubeAPI>(TYPES.YoutubeService).toConstantValue(
  new YoutubeService({
    clientId: env.YOUTUBE_CLIENT_ID,
    clientSecret: env.YOUTUBE_SECRET,
    redirectUris: JSON.parse(env.YOUTUBE_REDIRECT_URIS || "[]"),
    accessToken: {
      access_token: env.YOUTUBE_ACCESS_TOKEN,
      token_type: env.YOUTUBE_TOKEN_TYPE,
      refresh_token: env.YOUTUBE_REFRESH_TOKEN,
      expiry_date: Number(env.YOUTUBE_EXPIRACY_DATE),
    },
  })
);

container.bind<Velvet>(TYPES.Velvet).toConstantValue(
  new Velvet({
    token: env.VELVET_TOKEN,
    commandPrefix: env.COMMAND_PREFIX,
    owner: JSON.parse(env.OWNERS || "[]"),
  })
);
